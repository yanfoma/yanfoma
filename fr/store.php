<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>..::YanfoShop @ Yanfoma || The hotpot of new technologies::..</title>
    <?php include_once("librairies/meta.php"); ?>
</head>
<body>

<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>

<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3>Welcome TO YanfoShop</h3>
        </div><!-- /.box -->
    </div><!-- /.container -->
</div>

<div class="shop sec-padd">
    <div class="container">
        <div class="row">
            <?php include_once("librairies/sidebarStore.php"); ?>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart.html"><img src="images/shop/1.jpg" alt="Awesome Image"></a>

                            </div><!-- /.img-box -->
                            <div class="content-box">
                                <div class="inner-box">
                                    <h4><a href="shop-cart.html">The Innovators</a></h4>
                                    <div class="item-price">$34.99</div>
                                </div>
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart.html" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart.html"><img src="images/shop/2.jpg" alt="Awesome Image"></a>
                            </div><!-- /.img-box -->
                            <div class="content-box">
                                <div class="inner-box">
                                    <h4><a href="shop-cart.html">Win Your Friends</a></h4>
                                    <div class="item-price">$29.99</div>
                                </div>
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart.html" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart.html"><img src="images/shop/3.jpg" alt="Awesome Image"></a>
                            </div><!-- /.img-box -->
                            <div class="content-box">
                                <div class="inner-box">
                                    <h4><a href="shop-cart.html">Business Adventures</a></h4>
                                    <div class="item-price">$24.99</div>
                                </div>
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart.html" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart.html"><img src="images/shop/4.jpg" alt="Awesome Image"></a>
                            </div><!-- /.img-box -->
                            <div class="content-box">
                                <div class="inner-box">
                                    <h4><a href="shop-cart.html">Good to Great</a></h4>
                                    <div class="item-price">$29.00</div>
                                </div>
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart.html" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart.html"><img src="images/shop/5.jpg" alt="Awesome Image"></a>
                            </div><!-- /.img-box -->
                            <div class="content-box">
                                <div class="inner-box">
                                    <h4><a href="shop-cart.html">Built to Last</a></h4>
                                    <div class="item-price">$24.99</div>
                                </div>
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart.html" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart.html"><img src="images/shop/6.jpg" alt="Awesome Image"></a>
                            </div><!-- /.img-box -->
                            <div class="content-box">
                                <div class="inner-box">
                                    <h4><a href="shop-cart.html">Art of the Start</a></h4>
                                    <div class="item-price">$34.99</div>
                                </div>
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart.html" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart.html"><img src="images/shop/7.jpg" alt="Awesome Image"></a>
                            </div><!-- /.img-box -->
                            <div class="content-box">
                                <div class="inner-box">
                                    <h4><a href="shop-cart.html">Business Adventures</a></h4>
                                    <div class="item-price">$24.99</div>
                                </div>
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart.html" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart.html"><img src="images/shop/8.jpg" alt="Awesome Image"></a>
                            </div><!-- /.img-box -->
                            <div class="content-box">
                                <div class="inner-box">
                                    <h4><a href="shop-cart.html">Lords of Strategy</a></h4>
                                    <div class="item-price">$34.99</div>
                                </div>
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart.html" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart.html"><img src="images/shop/9.jpg" alt="Awesome Image"></a>
                            </div><!-- /.img-box -->
                            <div class="content-box">
                                <div class="inner-box">
                                    <h4><a href="shop-cart.html">Win Your Friends</a></h4>
                                    <div class="item-price">$29.00</div>
                                </div>
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart.html" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <br>
                <ul class="page_pagination center">
                    <li><a href="#" class="tran3s"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="active tran3s">1</a></li>
                    <li><a href="#" class="tran3s">2</a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
</div>

</body>
</html>