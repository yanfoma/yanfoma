<section class="blog-section sec-padd2">
    <div class="container">
        <div class="section-title center">
            <h2>Nos Récents Articles</h2>
        </div>
        <div class="row">
            <?php if($news->num_rows > 0){
                while( $news_row = $news->fetch_assoc() ){
            ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="single.php?post=<?php echo($news_row['id']); ?>">
                            <img src="<?php if($news_row['image'] ==""){
                                                echo $default_image;
                                            }else {
                                                echo "admin/uploads/$news_row[image]";
                                            } ?>" alt="Yanfoma News">
                        </a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="single.php?post=<?php echo($news_row['id']); ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date"><?php echo($news_row['date']);?></div>
                        <h4><a href="single.php?post=<?php echo($news_row['id']); ?>"><?php echo($news_row['title']);?></a></h4>
                        <div class="post-meta">by <span class="author"><?php echo($news_row['author'])?></span>  |  <span class="comments"><?php
                            $id             = $news_row['id'];
                            $query_comments = "SELECT * FROM comments WHERE post='$id' AND status='1'";
                            $comments       = $db->query($query_comments);
                            countComments($comments->num_rows);?></span>
                        </div>
                        <div class="text">
                            <p style="text-align:justify;"><?php $body = $news_row['body'];
                                    echo substr($body, 0, 100) ."...";
                                ?>
                            </p>
                        </div>
                        <div class="link">
                            <a href="single.php?post=<?php echo($news_row['id']); ?>" class="default_link">Lire Plus <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php }
            } else echo '<center><h3>Pas Darticles</h3></center>'; ?>
        </div>
    </div>
</section>