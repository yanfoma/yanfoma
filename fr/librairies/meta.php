<!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Google -->
    <meta name="description" content="Yanfoma est une entreprise, qui fournit des services technologiques et développe des produits innovants pour les organisations et innovateurs">
    <meta name="title" content="Yanfoma - Technologie et Produits Innovants">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">

    <link rel="apple-touch-icon" sizes="180x180" href="images/fav-icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="images/fav-icon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/fav-icon/favicon-16x16.png" sizes="16x16">

    <link rel="stylesheet" href="js/lightbox/css/videobox.css" type="text/css" media="screen" />
    <style>
    .quotation {
        display: inline-block;
        text-align: center;
        vertical-align: middle;
        padding: 0px 24px;
        border: 4px solid #ffffff;
        border-radius: 14px;
        background: #ffffff;
        background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#ffffff));
        background: -moz-linear-gradient(top, #ffffff, #ffffff);
        background: linear-gradient(to bottom, #ffffff, #ffffff);
        text-shadow: #ffffff 1px 1px 1px;
        font: normal normal bold 22px arial;
        color: #000000;
        text-decoration: none;
    }
    .quotation:hover,
    .quotation:focus {
        color: #000000;
        text-decoration: none;
    }
    .quotation:active {
        background: #b3b3b3;
        background: -webkit-gradient(linear, left top, left bottom, from(#b3b3b3), to(#ffffff));
        background: -moz-linear-gradient(top, #b3b3b3, #ffffff);
        background: linear-gradient(to bottom, #b3b3b3, #ffffff);
    }
    </style>
