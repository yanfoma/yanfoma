<footer class="main-footer">
    <!--Widgets Section-->
    <div class="widgets-section">
        <div class="container">
            <div class="row">
                <!--Big Column-->
                <div class="big-column col-md-7 col-sm-12 col-xs-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget about-widget">
                                <h3 class="footer-title">À propos de nous</h3>

                                <div class="widget-content">
                                    <div class="text"><p style="text-align:justify;"> Yanfoma est une jeune entreprise créée en septembre 2016, qui fournit des services technologiques et développe des produits innovants pour ceux qui ont une organisation (à but lucratif ou à but non lucratif) et qui ont besoin de technologie pour prendre des décisions, développer leurs activités et mesurer leurs performances, ceux qui ont un produit et/ou service technologique et ont besoin d'un nouveau marché et  ceux qui ont une idée technologique et veulent la développer.</p> </div>
                                    <div class="link">
                                        <a href="about.php" class="default_link">Lire Plus <i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <h3 class="footer-title">Nos Services</h3>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="services.php">Web</a></li>
                                        <li><a href="services.php">Applications Cloud & Sociales </a></li>
                                        <li><a href="services.php">Web Marketing & Référencement Web</a></li>
                                        <li><a href="services.php">Hébergement Web</a></li>
                                        <li><a href="services.php">Community Management</a></li>
                                        <li><a href="services.php">Vidéos et Affiches Publicitaires</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Big Column-->
                <div class="big-column col-md-5 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <!--Footer Column-->
                        <div class="footer-column col-md-12 col-sm-12 col-xs-12">
                            <div class="footer-widget contact-widget">
                                <h3 class="footer-title">Nous Contacter</h3>
                                <div class="widget-content">
                                    <ul class="contact-info">
                                         <li><span class="icon-signs"></span>Quartier Général, Taiwan New Taipei city, Banqiao district, Xianmian Boulevard, Alley 33, Lane 6, No 6</li>
                                        <li><span class="icon-phone-call"></span> Téléphone: +886 9895-97235</li>
                                        <li><span class="icon-e-mail-envelope"></span>info@yanfoma.tech</li>
                                    </ul>
                                </div>
                                <ul class="social">
                                    <li><a href="https://www.facebook.com/yanfoma/"                         target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://www.youtube.com/channel/UCXm7arljVJbdFIIEVdnt-LQ"  target="_blank"><i class="fa fa-youtube"></i></a></li>
                                    <li><a href="https://plus.google.com/u/0/100764898996932239647"         target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="https://www.linkedin.com/company-beta/22333242"            target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

             </div>
         </div>
     </div>

     <!--Footer Bottom-->
     <section class="footer-bottom">
        <div class="container">
            <div class="pull copy-text">
                <center><p>Copyrights © Dépuis 2016 Tous droits réservés. <a href="">Yanfoma</a></p></center>

            </div><!-- /.pull-right -->
        </div><!-- /.container -->
    </section></footer>
<!-- Scroll Top Button -->
<button class="scroll-top tran3s color2_bg">
    <span class="fa fa-angle-up"></span>
</button>
<!-- pre loader  -->
<div class="preloader"></div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51805819-8', 'auto');
  ga('send', 'pageview');

</script>
