<section class="rev_slider_wrapper">
    <div id="slider1" class="rev_slider"  data-version="5.0">
        <ul>

            <li data-transition="fade">
                <img src="../en/images/slider/1.png"  alt="" width="1920" height="683" data-bgposition="bottom center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >

                <div class="tp-caption tp-resizeme"
                    data-x="right" data-hoffset="160"
                    data-y="center" data-voffset="150"
                    data-transform_idle="o:1;"
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="500">
                    <div class="slide-content-box"></div>
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="left" data-hoffset="132"
                    data-y="center" data-voffset="245"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="about.php">Qui Sommes Nous?</a>
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="left" data-hoffset="837"
                    data-y="center" data-voffset="245"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="contact.php">NOUS CONTACTER</a>
                        </div>
                    </div>
                </div>
            </li>
            <li data-transition="ScaleUp">
                <img src="../en/images/slider/1.jpg"  alt="" width="1920" height="683" data-bgposition="bottom center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >

                <div class="tp-caption tp-resizeme"
                    data-x="right" data-hoffset="160"
                    data-y="center" data-voffset="150"
                    data-transform_idle="o:1;"
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="500">
                    <div class="slide-content-box">
                        <h1>La technologie pour tous !!! </h1>

                    </div>
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="left" data-hoffset="132"
                    data-y="center" data-voffset="245"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="about.php">Qui Sommes Nous?</a>
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="left" data-hoffset="837"
                    data-y="center" data-voffset="245"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="contact.php">NOUS CONTACTER</a>
                        </div>
                    </div>
                </div>
            </li>
            <li data-transition="Slide">
                <img src="../en/images/slider/2.jpg"  alt="" width="1920" height="683" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >

                <div class="tp-caption tp-resizeme"
                    data-x="center" data-hoffset="0"
                    data-y="center" data-voffset="-60"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="100">
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="center" data-hoffset=""
                    data-y="center" data-voffset="30"
                    data-transform_idle="o:1;"
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="500">
                    <div class="slide-content-box center">
                        <h1>Votre Expansion est notre objectif</h1>
                    </div>
                </div>

            </li>

            <li data-transition="fade">
                <img src="../en/images/slider/3.jpg"  alt="" width="1920" height="683" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >

                <div class="tp-caption  tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="top" data-voffset="240"
                    data-transform_idle="o:1;"
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="700">
                    <div class="slide-content-box">
                        <h1>Partenariat Strategique d'Affaires</h1>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="top" data-voffset="470"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="#">Qui Sommes Nous?</a>
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="left" data-hoffset="225"
                    data-y="top" data-voffset="470"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="contact.html">NOUS CONTACTER</a>
                        </div>
                    </div>
                </div>
            </li>
            <li data-transition="fade">
                <img src="../en/images/slider/4.png"  alt="" width="1920" height="683" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >

                <div class="tp-caption tp-resizeme"
                    data-x="left" data-hoffset="160"
                    data-y="center" data-voffset="0"
                    data-transform_idle="o:1;"
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="500">
                    <div class="slide-content-box">
                        <h1>Forfait Web complet <br>Hebergement, SEO, Marketing</h1>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="left" data-hoffset="132"
                    data-y="center" data-voffset="245"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="about.php">Qui Sommes Nous?</a>
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="left" data-hoffset="837"
                    data-y="center" data-voffset="245"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="contact.php">NOUS CONTACTER</a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
