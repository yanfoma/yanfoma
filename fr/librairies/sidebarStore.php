<div class="col-md-3 col-sm-12 col-xs-12 sidebar_styleTwo">
                <div class="wrapper">
                    <div class="sidebar_search">
                        <form action="#">
                            <input type="text" placeholder="Search....">
                            <button class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div> <br><br><!-- End of .sidebar_styleOne -->

                    <div class="category-style-one">
                        <div class="theme_inner_title">
                            <h3>Categories</h3>
                        </div>
                        <ul class="list">
                            <li><a href="#" class="clearfix"><span class="float_left">Business Growth </span><span class="float_right">(6)</span></a></li>
                            <li><a href="#" class="clearfix"><span class="float_left">Consulting </span><span class="float_right">(2)</span></a></li>
                            <li><a href="#" class="clearfix"><span class="float_left">Management  </span><span class="float_right">(5)</span></a></li>
                            <li><a href="#" class="clearfix"><span class="float_left">Customer Insights </span><span class="float_right">(10)</span></a></li>
                            <li><a href="#" class="clearfix"><span class="float_left">Organization  </span><span class="float_right">(4)</span></a></li>
                        </ul>
                    </div> <!-- End of .sidebar_categories -->

                    <div class="best_sellers clear_fix wow fadeInUp">
                        <div class="theme_inner_title">
                            <h3>Popular Products</h3>
                        </div>
                        <div class="best-selling-area">
                            <div class="best_selling_item clear_fix border">
                                <div class="img_holder float_left">
                                    <a href="shop-single.html"><img src="images/shop/11.jpg" alt="image"></a>
                                </div> <!-- End of .img_holder -->

                                <div class="text float_left">
                                    <a href="shop-single.html"><h4>The Innovators</h4></a>
                                    <span>$34.99</span>
                                    <ul>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                    </ul>
                                </div> <!-- End of .text -->
                            </div> <!-- End of .best_selling_item -->

                            <div class="best_selling_item clear_fix border">
                                <div class="img_holder float_left">
                                    <a href="shop-single.html"><img src="images/shop/12.jpg" alt="image"></a>
                                </div> <!-- End of .img_holder -->

                                <div class="text float_left">
                                    <a href="shop-single.html"><h4>Good to Great</h4></a>
                                    <span>$24.00</span>
                                    <ul>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    </ul>
                                </div> <!-- End of .text -->
                            </div> <!-- End of .best_selling_item -->

                            <div class="best_selling_item clear_fix">
                                <div class="img_holder float_left">
                                    <a href="shop-single.html"><img src="images/shop/13.jpg" alt="image"></a>
                                </div> <!-- End of .img_holder -->

                                <div class="text float_left">
                                    <a href="shop-single.html"><h4>Built to Last</h4></a>
                                    <span>$20.00</span>
                                    <ul>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                    </ul>
                                </div> <!-- End of .text -->
                            </div> <!-- End of .best_selling_item -->
                        </div>
                            
                    </div> <!-- End of .best_sellers -->


                    <div class="sidebar_tags wow fadeInUp">
                            <div class="theme_inner_title">
                            <h3>Tag Cloud</h3>
                        </div>

                        <ul>
                            <li><a href="#" class="tran3s">Book</a></li>
                            <li><a href="#" class="tran3s">Company</a></li>
                            <li><a href="#" class="tran3s">Ideas</a></li>
                            <li><a href="#" class="tran3s">Energy</a></li>
                            <li><a href="#" class="tran3s">Engines</a>  </li>
                            <li><a href="#" class="tran3s">Chemical</a></li>
                            <li><a href="#" class="tran3s">Industry</a> </li>
                            <li><a href="#" class="tran3s">Research</a></li>
                        </ul>
                    </div> <!-- End of .sidebar_tags -->

                </div> <!-- End of .wrapper -->
            </div>