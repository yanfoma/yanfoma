<section class="service sec-padd2">
    <div class="container">
        <div class="section-title">
            <h2>Nos Services</h2>
        </div>
        <div class="service_carousel">
            <!--Featured Service -->
            <article class="single-column">
                <div class="item">
                    <figure class="img-box">
                        <img src="images/service/private-incubation.jpg" alt="">
                        <figcaption class="default-overlay-outer">
                            <div class="inner">
                                <div class="content-layer">
                                    <a href="#" class="thm-btn thm-tran-bg"></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="content center">
                        <a href="#"><h4>Marketing  <br>Numérique</h4></a>
                        <div class="text">
                            <p></p>
                        </div>
                    </div>
                </div>
            </article>
            <!--Featured Service -->
            <article class="single-column">
                <div class="item">
                    <figure class="img-box">
                        <img src="images/service/strategic-business-partnership.jpg" alt="">
                        <figcaption class="default-overlay-outer">
                            <div class="inner">
                                <div class="content-layer">
                                    <a href="#" class="thm-btn thm-tran-bg"></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="content center">
                        <a href="#"><h4> Partenariat d'affaires</h4></a>
                        <div class="text">
                            <p></p>
                        </div>
                    </div>
                </div>
            </article>
            <!--Featured Service -->
            <article class="single-column">
                <div class="item">
                    <figure class="img-box">
                        <img src="images/service/SEO.jpg" alt="">
                        <figcaption class="default-overlay-outer">
                            <div class="inner">
                                <div class="content-layer">
                                    <a href="#" class="thm-btn thm-tran-bg"></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="content center">
                        <a href="#"><h4>Web Marketing &amp; SEO, Hosting </h4></a>
                        <div class="text">
                            <p></p>
                        </div>
                    </div>
                </div>
            </article>
            <!--Featured Service -->
            <article class="single-column">
                <div class="item">
                    <figure class="img-box">
                        <img src="images/service/videos-and-posters.jpg" alt="">
                        <figcaption class="default-overlay-outer">
                            <div class="inner">
                                <div class="content-layer">
                                    <a href="media.php" class="thm-btn thm-tran-bg"></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="content center">
                        <a href="media.php"><h4>Publicité <br> Videos & Posters</h4></a>
                        <div class="text">
                            <p></p>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</section>
