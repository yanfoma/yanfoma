<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>..::Services Yanfoma || The hotpot of new technologies::..</title>
    <?php include_once("librairies/meta.php"); ?>
</head>
<style type="text/css">
    .img-box{
        border:#eee 2px solid;
    }
</style>
<body>
<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<section class="service style-2 sec-padd2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-8 col-sm-12">
                <div class="row">
                    <article class="column col-md-4 col-sm-6 col-xs-12">
                        <div class="item">
                            <figure class="img-box">
                                <img src="images/service/private-incubation.jpg" alt="">
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer">
                                            <a href="privateIncubation.php" class="thm-btn thm-tran-bg">Lire Plus</a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="content center">
                                <a href="#"><h4>Marketing Numérique et Community management</h4></a>
                                <div class="text">
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="column col-md-4 col-sm-6 col-xs-12">
                        <div class="item">
                            <figure class="img-box">
                                <img src="images/service/social-networks.jpg" alt="">
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer">
                                            <a href="web.php" class="thm-btn thm-tran-bg">Lire Plus</a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="content center">
                                <a href="web.php"><h4>Cloud &amp; Applications Sociales</h4></a>
                                <div class="text">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="column col-md-4 col-sm-6 col-xs-12">
                        <div class="item">
                            <figure class="img-box">
                                <img src="images/service/SEO.jpg" alt="">
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer">
                                            <a href="web.php" class="thm-btn thm-tran-bg">Lire Plus</a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="content center">
                                <a href="web.php"><h4> Marketing Web &amp; SEO, Hébergement </h4></a>
                                <div class="text">
                                    <p> <br>  <br></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="column col-md-4 col-sm-6 col-xs-12">
                        <div class="item">
                            <figure class="img-box">
                                <img src="images/service/strategic-business-partnership.jpg" alt="">
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer">
                                            <a href="strategicBusinessPartnership.php" class="thm-btn thm-tran-bg">read more</a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="content center">
                                <a href="strategicBusinessPartnership.php"><h4>Partenariat d'affaires stratégiques</h4></a>
                                <div class="text">
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="column col-md-4 col-sm-6 col-xs-12">
                        <div class="item">
                            <figure class="img-box">
                                <img src="images/service/community-management.jpg" alt="">
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer">
                                            <a href="web.php" class="thm-btn thm-tran-bg">Lire Plus</a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="content center">
                                <a href="web.php"><h4>Gestion de communauté</h4></a>
                            </div>
                        </div>
                    </article>

                    <article class="column col-md-4 col-sm-6 col-xs-12">
                        <div class="item">
                            <figure class="img-box">
                                <img src="images/service/videos-and-posters.jpg" alt="">
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer">
                                            <a href="media.php" class="thm-btn thm-tran-bg">Lire Plus</a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="content center">
                                <a href="media.php"><h4>Publicité <br> Videos et Posters</h4></a>
                                <div class="text">
                                </div>
                            </div>
                        </div>
                    </article>

                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
</div>

</body>
</html>
