<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>YanfoMedia || Publicité, Videos et Posters</title>
    <?php include_once("librairies/meta.php"); ?>
    <link rel="stylesheet" type="text/css" href="js/video/content/global.css">
</head>
<body>

<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<?php include_once("librairies/imagePlaylist.php"); ?>
<section class="whychoos-us sec-padd2">
    <div class="container">

        <div class="section-title center">
            <h2>Votre WOW !!! est notre objectif. </h2>
            <div class="text">
                <p></p>
            </div>
        </div>

        <div class="row clearfix">
            <!--Featured Service -->
            <article class="column col-md-6 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-science-1"></span>
                    </div>
                    <a href="#"><h4>Vidéos Publicitaires</h4></a>
                    <div class="text">
                        <p>Vous avez un nouveau produit ou une entreprise que vous souhaitez promouvoir à travers des spots vidéo? Nous sommes le meilleur et votre satisfaction est notre devoir!!!!</p>
                    </div>
                    <div class="count">01</div>
                </div>
            </article>
            <!--Featured Service -->
            <article class="column col-md-6 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-computer"></span>
                    </div>
                    <a href="#"><h4>Posters Et Logos</h4></a>
                    <div class="text">
                        <p>Vous planifiez un événement et vous cherchez l'affiche correcte pour attirer les gens? Ne Cherchez plus, nous sommes la.</p>
                    </div>
                    <div class="count">02</div>
                </div>
            </article>
        </div>
    </div>
</section>
<?php include_once("librairies/playlistMedia.php"); ?>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
</div>
</body>
</html>