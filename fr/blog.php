<?php
    include("../en/librairies/config.php");
    include("../en/librairies/db.php");
    if(isset($_GET['category'])){
        $category = mysqli_real_escape_string($db, $_GET['category']);
        $query = "SELECT * FROM posts WHERE category='$category'";

        $cat=$db->query("SELECT * FROM categories WHERE id='$category'");
        $c          =$cat->fetch_assoc();
        $page_title = $c['name']." Posts";
    }
    else{

        $query = "SELECT * FROM posts";
    }
    $posts= $db->query($query);
    /*****************Pagination************************/
    //The number of posts in the table
    $total_posts    = $posts->num_rows;
    $page_rows       = 3;
    $last   = ceil($total_posts / $page_rows);
    if($last < 1) {
        $last = 1;
    }
    $pagenum = 1;
    if(isset($_GET['pn'])){
        $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
    }
    if ($pagenum < 1) {
    $pagenum = 1;

    } else if ($pagenum > $last) {
        $pagenum = $last;
    }
    $limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
    $sql   = "SELECT * FROM posts  $limit";

    $posts_filtrer= $db->query($sql);
    $textline = "Page <b> $pagenum</b> of <b>$last</b>";
    $paginationCtrls = '';

    if($last != 1){

        if($pagenum > 1){
            $previous = $pagenum - 1;
            $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'" class="tran3s"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li> &nbsp; &nbsp; ';

            for($i = $pagenum-4; $i < $pagenum; $i++){
                if($i > 0){
                    $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="tran3s">'.$i.'</a></li> &nbsp; ';
                }
            }
        }
    $paginationCtrls .= ''.$pagenum.' &nbsp; ';

    for($i = $pagenum+1; $i <= $last; $i++){
        $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="tran3s">'.$i.'</a></li> &nbsp; ';
        if($i >= $pagenum+4){
            break;
        }
    }

     if ($pagenum != $last) {

        $next = $pagenum + 1;
        $paginationCtrls .= ' &nbsp; &nbsp; <li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'" class="tran3s"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>';

    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>..::<?php echo $page_title;?>|| The hotpot of new technologies::..</title>
    <?php include_once("librairies/meta.php"); ?>
</head>
<body>

<div class="boxed_wrapper">
    <?php include_once("librairies/header.php"); ?>
    <div class="inner-banner text-center">
        <div class="container">
            <div class="box">
                <h3>Bienvenue sur YanfoBlog</h3>
            </div><!-- /.box -->
        </div><!-- /.container -->
    </div>

    <?php //include_once("librairies/sidebar.php"); ?>
    <section class="blog-section sec-padd">
        <div class="container">
            <div class="row">
                <?php
                    while($row = $posts_filtrer->fetch_assoc() ){
                        ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                                <figure class="img-holder">
                                     <a href="single.php?post=<?php echo($row['id']); ?>">
                                         <img src="<?php if($row['image']== "")
                                         {
                                            echo $default_image;
                                         }else{
                                            echo"admin/uploads/$row[image]";
                                         }
                                        ?>" alt="News">
                                     </a>
                                     <figcaption class="overlay">
                                         <div class="box">
                                             <div class="content">
                                                 <a href="single.php?post=<?php echo($row['id']); ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                                             </div>
                                         </div>
                                     </figcaption>
                                </figure>
                                <div class="lower-content">
                                     <div class="date"><?php echo($row['date'])?></div>
                                     <h4><a href="single.php?post=<?php echo($row['id']); ?>"><?php echo($row['title'])?></a></h4>
                                    <div class="post-meta">
                                        par
                                        <span class="author">
                                            <?php
                                                 if($row['author']="") echo $default_author;
                                                 else                  echo($row['author']);
                                            ?>
                                        </span>
                                        |  Tag:
                                            <?php
                                                if($row['keywords']=="") echo $default_keyword;
                                                else                     echo($row['keywords']);
                                            ?>
                                        |   categorie:
                                            <?php
                                                if($row['category']=='0') echo $default_category;
                                                else                      echo($row['category']);
                                            ?>
                                    </div>
                                    <div class="text">
                                        <p style="text-align:justify;">
                                            <?php $body = $row['body'];
                                                echo substr($body, 0, 100) ."...";
                                            ?>
                                        </p>
                                    </div>
                                    <div class="link">
                                         <a href="single.php?post=<?php echo($row['id']); ?>" class="default_link">Lire Plus <i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                 <?php }?>
            </div>
        </div>
        <ul class="page_pagination center">
            <?php echo $paginationCtrls;?>
        </ul>
    </section>
    <?php //include_once("librairies/newsletter.php"); ?>
    <?php include_once("librairies/footer.php"); ?>
    <?php include_once("librairies/script.php"); ?>

</div>

</body>
</html>