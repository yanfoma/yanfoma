<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>..::About Us @ Yanfoma || The hotpot of new technologies::..</title>
    <?php include_once("librairies/meta.php"); ?>
    <link rel="stylesheet" href="js/lightbox/css/videobox.css" type="text/css" media="screen" />
</head>
<body>
<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<section class="whychoos-us sec-padd2">
    <div class="container">

        <div class="section-title center">
            <h2>Qui nous sommes</h2>
            <div class="text">
                <p>Yanfoma est une jeune entreprise créée en septembre 2016, qui fournit des services technologiques et développe des produits innovants pour ceux qui ont une organisation (à but lucratif ou à but non lucratif) et qui ont besoin de technologie pour prendre des décisions, développer leurs activités et mesurer leurs performances, ceux qui ont un produit et/ou service technologique et ont besoin d'un nouveau marché et  ceux qui ont une idée technologique et veulent la développer. Yanfoma, ayant acquis des expériences, des partenaires, des opportunités, s'est rapidement transformé en une jeune entreprise qui opère à distance à Ouagadougou (Burkina Faso). Yanfoma se considère comme une entreprise africaine, élargissant ses activités au Nigeria et au Rwanda dans les prochains mois.</p>
            </div>
        </div>
        <div class="row clearfix">
            <!--Featured Service -->
            <article class="column col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-science-1"></span>
                    </div>
                    <a href="#"><h4>Votre expansion est notre objectif</h4></a>
                    <div class="text">
                        <p> Nous offrons des services pour ceux qui ont une organisation (à but lucratif ou à but non lucratif) et qui ont besoin de technologie pour prendre des décisions, développer leurs activités et mesurer leurs performances. Nous priorisons la technologie et nos clients priorise leurs activites, nous developpons ce dont ils savent ils ont besoin et les conseillons sur ce dont ils peuvent avoir besoin</p>
                    </div>
                    <div class="count">01</div>
                </div>
            </article>
            <!--Featured Service -->
            <article class="column col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-computer"></span>
                    </div>
                    <a href="#"><h4>Des opportunites de nouveaux marché pour votre business</h4></a>
                    <div class="text">
                        <p>
                          Nous offrons des services pour ceux qui ont un produit et/ou service technologique et ont besoin d'un nouveau marché. Nous aidons nos clients à adapter leurs produits pour attirer leur consommateurs, à reduire les coûts d'expansion et à reduire les efforts de marketing des produits</p>
                    </div>
                    <div class="count">02</div>
                </div>
            </article>
            <!--Featured Service -->
            <article class="column col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-money2"></span>
                    </div>
                    <a href="#"><h4>Travaillons ensemble pour faire de votre idée une realité</h4></a>
                    <div class="text">
                        <p>
                          Nous offrons des services pour ceux qui ont une idée technologique et veulent la développer. Nous aidons nos clients en les mettant en contact avec des professionels de la technologie, en les mettant en contact avec des makers, des fabriquants, des développeurs, tout cela pour faire des idées de nos innovateurs un realité<br><br><br< p="">
                    </br<></p></div>
                    <div class="count">03</div>
                </div>
            </article>
        </div>
    </div>
</section>
<!--section class="default-section sec-padd">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="text-content">
                    <h4>We make it true</h4>
                    <div class="text">
                        <p>Vous avez des rêves à propos de la prochaine fantastique application ou du produit génial qui vous permettra de résoudre des problèmes et gagner de l'argent. Yanfoma est là pour que votre idée devienne réalité, pour vous aider à mieux gérer votre business et ainsi contribuer à avoir une communauté plus heureuse. Tout cela grâce aux technologies.</p>
                    </div>
                    <ul class="default-list">
                        <li><i class="fa fa-check-square-o"></i>Which toil and pain can procure great pleasure.</li>
                        <li><i class="fa fa-check-square-o"></i>Any right to find man who annoying.</li>
                        <li><i class="fa fa-check-square-o"></i>Consequences, avoids a pain that produces.</li>
                    </ul>
                    <div class="text">
                        <p>Who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <figure class="img-box">
                    <a href="#"><img src="images/resource/12.jpg" alt=""></a>
                </figure>
            </div>
        </div>
    </div>
</section-->
<section class="parallax center" style="background-image: url(images/background/1.jpg);">
    <div class="container">
        <h1>Découvrez Yanfoma En quelques minutes !!!</h1>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="video-holder">
            <div class="overlay-gallery">
                <div class="icon-holder">
                    <div class="icon">
                        <a class="html5lightbox thm-btn" title="Yanfoma Introduction" href="https://www.youtube.com/watch?v=-xDcvGWgTNk">Voir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="service-style3 sec-padd2">
    <div class="container">
        <div class="section-title">
            <h2>Comment Ça Ce Passe</h2>
        </div>
        <div class="service_carousel">
            <div class="item">
                <div class="bottom-content">
                    <div class="icon_box">
                        <span class="icon-platform"></span>
                    </div>
                    <a href="#"><h4>IDEE</h4></a>
                    <p class="title">Innovante</p>
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                        <span class="icon-platform"></span>
                        </div>
                        <a href="#"><h4>IDEE</h4></a>
                        <div class="text">
                            <p>Si vous avez une idée intéressante que vous souhaitez réaliser ou prototype, vous pouvez vous joindre à nous.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="bottom-content">
                    <div class="icon_box">
                        <span class="icon-phone-call"></span>
                    </div>
                    <a href="#"><h4>Contactez Nous</h4></a>
                    <p class="title">N'Hesitez Pas</p>
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                        <span class="icon-phone-call"></span>
                        </div>
                        <a href="#"><h4>Contactez Nous</h4></a>
                        <div class="text">
                            <p>Entrez en contact avec nous par email et joignez votre idée. Nous vous donnerons quelques conseils et analyserons la possibilité de vous joindre à yanfoma.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="bottom-content">
                    <div class="icon_box">
                        <span class="icon-people"></span>
                    </div>
                    <a href="#"><h4>Rejoignez-nous</h4></a>
                    <p class="title">Faites partie de Yanfoma</p>
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                        <span class="icon-people"></span>
                        </div>
                        <a href="#"><h4>Rejoignez-nous</h4></a>
                        <div class="text">
                            <p>Lorsque vous vous joignez à yanfoma, vous bénéficiez d'un soutien total et devenez le gestionnaire de votre projet. Donc, vous devenez le patron, nous vous guidons.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="bottom-content">
                    <div class="icon_box">
                        <span class="icon-business"></span>
                    </div>
                    <a href="#"><h4>Collaborons</h4></a>
                    <p class="title">Partenariat d'affaires</p>
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                        <span class="icon-business"></span>
                        </div>
                        <a href="#"><h4>Collaborons</h4></a>
                        <div class="text">
                            <p>Nous pouvons signer un partenariat afin de nous aider mutuellement à développer des entreprises locales ou internationales. Nous encourageons le partenariat gagnant-gagnant</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php //include_once("librairies/faq.php"); ?>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
    <!-- Html 5 light box script-->
<script src="js/html5lightbox/html5lightbox.js"></script>
</div>
</body>
</html>
