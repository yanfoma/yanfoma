<?php
    include("librairies/config.php");
    include("librairies/db.php");

    if(isset($_GET['post'])){
        $id = mysqli_real_escape_string($db, $_GET['post']);
        $query = "SELECT * FROM posts WHERE id='$id'";

        $post=$db->query($query);
        $p          =$post->fetch_assoc();
        $page_title = $p['title']." Posts";
    }
    else{

        $query = "SELECT * FROM posts";
    }

    $posts= $db->query($query);

    if(isset($_POST['post_comment'])){
        $name       = mysqli_real_escape_string($db, $_POST['post_name']);
        $comment    = mysqli_real_escape_string($db, $_POST['comment']);

        if(isset($_POST['post_email'])){
            $email  = mysqli_real_escape_string($db,$_POST['post_email']);
        }else{
            $email  = "";
        }

        $query2 = "INSERT INTO comments (name,comment,post,email,dateComment) VALUES('$name','$comment','$id','$email',CURRENT_TIMESTAMP())";

        if($db->query($query2)){
            header("Location:single.php?post=$id");
            exit();
        } else{
            echo"<script>alert('Commentaire non inséré!!!')</script>";
        }
    }

    $query_comments = "SELECT * FROM comments WHERE post='$id' AND status='1'";
    $comments = $db->query($query_comments);

    function time_diff_string($from, $to, $full = false) {
    $from   = new DateTime($from);
    $to     = new DateTime($to);
    $diff   = $to->diff($from);
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
function checkIsAdmin($is_admin){
    if($is_admin == 1)
    {
        $admin ="Admin";
    }else{
        $admin ="";
    }
    echo($admin);
}
function setCommentImage($is_admin){
    if($is_admin == 1){
        $imagePath="../en/images/comments/admin.png";
    }
    else{
        $imagePath="../en/images/comments/user.png";
    }
    echo $imagePath;
}

function countComments($count){
    if($count == 0 || $count == 1) echo $count.' Comment';
        else echo $count.' Commentaires';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $page_title;?>| Yanfoma The hotpot of new technologies</title>
    <?php include_once("librairies/meta.php"); ?>
</head>
<body>

<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<div class="sidebar-page-container sec-padd-top">
    <div class="container">
        <div class="row">
            <?php if($posts->num_rows > 0){
                while( $row = $posts->fetch_assoc() ){
                    ?>
                        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                            <section class="blog-section">
                                <div class="large-blog-news single-blog-post single-blog wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                    <div class="date"><?php echo($row['date'])?></div>
                                    <figure class="img-holder">
                                        <img src="<?php if($row['image']== "")
                                         {
                                            echo $default_image;
                                         }else{
                                            echo"../en/admin/uploads/$row[image]";
                                         }
                                        ?>" alt="News">
                                    </figure>
                                    <div class="lower-content">
                                        <h4><a href="#"><?php echo($row['title'])?></a></h4>
                                        <div class="post-meta">by <span class="author"><?php echo($row['author'])?></spam>  <span class="comments"> Commentaires</span></div>
                                        <div class="text">
                                            <p style="text-align:justify;"><?php echo $row['body'];?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="outer-box">
                                    <div class="share-box clearfix">
                                        <ul class="tag-box pull-left">
                                            <li>Tags:</li>
                                                <?php echo($row['keywords'])?>
                                        </ul>
                                        <div class="social-box pull-right">
                                        <span>Share <i class="fa fa-share-alt"></i></span>
                                        <ul class="list-inline social">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php }
                                    }?>
                                    <div class="product-review-tab">
                                        <div class="inner-title">
                                            <div class="post-meta"><span class="comments"><?php countComments($comments->num_rows);?></span> </div>
                                        </div>
                                        <div class="review-box">
                                            <!--Start single review box-->
                                                <?php
                                                while($comment = $comments->fetch_assoc()){
                                                    ?>
                                            <div class="single-review-box">
                                                <div class="img-holder">
                                                    <img src="<?php setCommentImage($comment['is_admin']);?>" alt="Awesome Image">
                                                </div>
                                                <div class="text-holder">
                                                    <div class="top">
                                                        <div class="name pull-left">
                                                            <h4><span class="name"><?php echo $comment['name'].' '; ?></span><a href="#" class="author"><?php checkIsAdmin($comment['is_admin']);echo '<br>';?></a></h4>
                                                        </div>
                                                    </div>
                                                    <div class="text">
                                                        <p><?php echo $comment['comment']; ?></p>
                                                        <span><?php
                                                                $dateComment = $comment['dateComment'];
                                                                $dateNow = new DateTime('now', new DateTimeZone('Asia/Taipei'));
                                                                echo time_diff_string($dateComment, $dateNow->format('Y-m-d H:i'));
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php }?>
                                            <!--End single review box-->
                                        </div>
                                        <div class="add_your_review">
                                            <div class="inner-title">
                                                <h3>Commenter</h3>
                                            </div>
                                            <div class="default-form-area">
                                                <form id="contact-form" name="contact_form" class="default-form" method="post">
                                                    <div class="row clearfix">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                                            <div class="form-group commentaire">
                                                                <input type="text" name="post_name" class="form-control" value="" placeholder="Votre Nom *" required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group commentaire">
                                                                <input type="email" name="post_email" class="form-control required email" value="" placeholder="Votre Mail *" required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group commentaire">
                                                                <textarea name="comment" class="form-control textarea required" placeholder="Votre Message...."></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <button class="thm-btn"   name="post_comment" type="submit" data-loading-text="Veuillez patienter...">Commenter</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </form>
                                            </div>
                                        </div> <!-- End of .add_your_review -->
                                    </div>
                                </div>
                            </section>
                        </div>
            <?php include_once("librairies/sidebar.php"); ?>
        </div>
    </div>
</div>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
</div>

</body>
</html>