<?php
include('header.php');
?>
<title>Contrat De Stage Yanfoma</title>
<script type="text/javascript" src="js/form.js"></script>
<style type="text/css">
body{
	font-family: Cambria, Palatino, "Palatino Linotype", "Palatino LT STD", Georgia, serif;
	background: #fff url(img/bg.jpg) repeat top left;
	font-weight: 400;
	font-size: 15px;
	overflow-y: scroll;
}
::-webkit-input-placeholder  {
	color: #000000;
	font-style: italic;
}
input:-moz-placeholder,
textarea:-moz-placeholder{
	color: #000000;
	font-style: italic;
}
.input2{
	border: 1px solid rgb(178, 178, 178);
	-webkit-appearance: textfield;
	-webkit-border-radius: 5px;
	   -moz-border-radius: 5px;
	        border-radius: 5px;
	-webkit-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
	   -moz-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
	        box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
	-webkit-transition: all 0.2s linear;
	   -moz-transition: all 0.2s linear;
	     -o-transition: all 0.2s linear;
	        transition: all 0.2s linear;
}
.input2:active,
.input2:focus{
	border: 1px solid rgba(91, 90, 90, 0.7);
	background: rgba(238, 236, 240, 0.2);
	-webkit-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.9) inset;
	   -moz-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.9) inset;
	        box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.9) inset;
}

p.change_link{
	position: absolute;
	color: rgb(127, 124, 124);
	z-index: 0;
	left: 0px;
	bottom: 0px;
	height: 30px;
	width: 100%;
	text-align: right;
	border-top: 1px solid rgb(219, 229, 232);
	-webkit-border-radius: 0 0  5px 5px;
	   -moz-border-radius: 0 0  5px 5px;
	        border-radius: 0 0  5px 5px;
	background: rgb(225, 234, 235);
	background: -moz-repeating-linear-gradient(-45deg,
	rgb(247, 247, 247) ,
	rgb(247, 247, 247) 15px,
	rgb(225, 234, 235) 15px,
	rgb(225, 234, 235) 30px,
	rgb(247, 247, 247) 30px
	);
	background: -webkit-repeating-linear-gradient(-45deg,
	rgb(247, 247, 247) ,
	rgb(247, 247, 247) 15px,
	rgb(225, 234, 235) 15px,
	rgb(225, 234, 235) 30px,
	rgb(247, 247, 247) 30px
	);
	background: -o-repeating-linear-gradient(-45deg,
	rgb(247, 247, 247) ,
	rgb(247, 247, 247) 15px,
	rgb(225, 234, 235) 15px,
	rgb(225, 234, 235) 30px,
	rgb(247, 247, 247) 30px
	);
	background: repeating-linear-gradient(-45deg,
	rgb(247, 247, 247) ,
	rgb(247, 247, 247) 15px,
	rgb(225, 234, 235) 15px,
	rgb(225, 234, 235) 30px,
	rgb(247, 247, 247) 30px
	);
}
  #register_form fieldset:not(:first-of-type) {
    display: none;
  }
  .kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
.logo{
	position: relative;
	width: auto;
}
#register_form{
	position: relative;
	padding: 18px 6% 60px 6%;
	margin: 0 0 35px 0;
	color: black;
	background: rgb(247, 247, 247);
	border: 1px solid rgba(147, 184, 189,0.8);
	-webkit-box-shadow: 0pt 2px 5px rgba(105, 108, 109,  0.7),	0px 0px 8px 5px rgba(208, 223, 226, 0.4) inset;
	   -moz-box-shadow: 0pt 2px 5px rgba(105, 108, 109,  0.7),	0px 0px 8px 5px rgba(208, 223, 226, 0.4) inset;
	        box-shadow: 0pt 2px 5px rgba(105, 108, 109,  0.7),	0px 0px 8px 5px rgba(208, 223, 226, 0.4) inset;
	-webkit-box-shadow: 5px;
	-moz-border-radius: 5px;
		 border-radius: 5px;
}

.next-form,.previous-form,.submit{
	width:auto;
	position: relative;
	z-index: 1;
	cursor: pointer;
	background: rgb(61, 157, 179);
	padding: 2px 5px;
	font-family: 'BebasNeueRegular','Arial Narrow',Arial,sans-serif;
	color: #fff;
	font-size: 18px;
	border: 1px solid rgb(28, 108, 122);
	margin-bottom: 10px;
	text-shadow: 0 1px 1px rgba(0, 0, 0, 0.5);
	-webkit-border-radius: 3px;
	   -moz-border-radius: 3px;
	        border-radius: 3px;
	-webkit-box-shadow: 0px 1px 6px 4px rgba(0, 0, 0, 0.07) inset,
	        0px 0px 0px 3px rgb(254, 254, 254),
	        0px 5px 3px 3px rgb(210, 210, 210);
	   -moz-box-shadow:0px 1px 6px 4px rgba(0, 0, 0, 0.07) inset,
	        0px 0px 0px 3px rgb(254, 254, 254),
	        0px 5px 3px 3px rgb(210, 210, 210);
	        box-shadow:0px 1px 6px 4px rgba(0, 0, 0, 0.07) inset,
	        0px 0px 0px 3px rgb(254, 254, 254),
	        0px 5px 3px 3px rgb(210, 210, 210);
	-webkit-transition: all 0.2s linear;
	   -moz-transition: all 0.2s linear;
	     -o-transition: all 0.2s linear;
	        transition: all 0.2s linear;
}
.next-form::hover{
	background: rgb(74, 179, 198);
}
.previous-form::hover{
	background: rgb(74, 179, 198);
}

</style>

<body>
<div class="container kv-main">
	<center><h2 style="color:red;">Merci de bien vouloir remplir tous les champs.</h2></center>
	<hr>
	<center><p style="color:red;"></p></center>
	<div class="progress">
	<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
	</div>
	<div class="alert alert-danger hide"></div>
	<form id="register_form" novalidate action="form_action.php"  method="post" enctype="multipart/form-data" onsubmit="return verifForm(this)">
		<fieldset>
		<h2>Etape 1</h2>
		<div class="row">
	        <div class="col-sm-12">
	          	<div class="row">
		            <div class="col-sm-6">
		              <div class="form-group ">
							<label for="civilite">civilité </label>
							<select name="civilite" class="form-control input2" onblur="verifInput(this)">
								<option value="" selected>civilité</option>
								<option value="Monsieur">Mr</option>
								<option value="Mademoiselle">Mlle</option>
								<option value="Madame">Mme</option>
							</select>
						</div>
		            </div>
		            <div class="col-sm-6">
		             	<div class="form-group">
							<label for="statut">Statut Matrimonial </label>
							<select name="statut" class="form-control input2" onblur="verifInput(this)">
								<option value="" selected>Votre Statut</option>
								<option value="Celibataire Sans Enfant">Célibataire Sans Enfant</option>
								<option value="Célibataire Avec Enfant">Célibataire Avec Enfant</option>
								<option value="Marié(e)">Marié(e)</option>
								<option value="Divorcé(e)">Divorcé(e)</option>
								<option value="Veuf/Veuve">Veuf/Veuve</option>
							</select>
						</div>
		            </div>
	          	</div>
	          	<div class="row">
		            <div class="col-sm-6">
		                <div class="form-group">
							<label for="nom">Nom</label>
							<input type="text" class="form-control input2" name="nom" id="nom" onblur="verifInput(this)" placeholder="Nom" required>
						</div>
		            </div>
		            <div class="col-sm-6">
		              <div class="form-group">
							<label for="prenoms"> Prénoms </label>
							<input type="text" class="form-control input2" name="prenoms" id="prenoms" onblur="verifInput(this)" placeholder="Prénoms" required>
						</div>
		            </div>
	          	</div>
	          	<div class="row">
		            <div class="col-sm-3">
		                <div class="form-group">
							<label for="dateOfBirth">Né(e) le : </label>
							<input type="text" class="form-control input2" name="dateOfBirth" id="dateOfBirth" onblur="verifInput(this)" placeholder="01/01/1900"  maxlength="10" required/>
						</div>
		            </div>
		            <div class="col-sm-3">
		              	<div class="form-group">
							<label for="placeOfBirth">À : </label>
							<input type="text" class="form-control input2" name="placeOfBirth" id="placeOfBirth" onblur="verifInput(this)" placeholder="Ouagadougou"  required/>
						</div>
		            </div>
		            <div class="col-sm-6">
		              	<div class="form-group">
							<label for="nationalite">Nationalité:</label>
							<input type="text" class="form-control input2" name="nationalite" id="nationalite" onblur="verifInput(this)" placeholder="Burkinabé"  required/>
						</div>
		            </div>
		            <div class="col-sm-6">
		              	<div class="form-group">
							<label for="domicile">Domicile </label>
							<select name="domicile" class="form-control input2" onblur="verifInput(this)">
								<option value="" selected>Ville</option>
								<option value="Ouagadougou">Ouagadougou</option>
								<option value="Bobo">Bobo Dioulasso</option>
								<option value="Koudougou">Koudougou</option>
								<option value="Kaya">Kaya</option>
								<option value="Banfora">Banfora</option>
							</select>
						</div>
		            </div>
		            <div class="col-sm-6">
		              	<div class="form-group">
							<label for="quartier">Quartier: </label>
							<input type="text" class="form-control input2" name="quartier" id="quartier" onblur="verifInput(this)" placeholder="1200 Logements" required />
						</div>
		            </div>
					<div class="col-sm-6">
		                <div class="form-group">
							<label for="age">Age</label>
							<input type="text" class="form-control input2" name="age" id="age" onblur="verifAge(this)" placeholder="Age" required>
						</div>
		            </div>
		            <div class="col-sm-6">
		            	<div class="row">
		            		<div class="col-sm-6">
			              		<div class="form-group ">
									<label for="position">Position </label>
									<select name="position" class="form-control input2" onblur="verifInput(this)" >
									<option value="" selected>Votre Position A Yanfoma</option>
										<option value="Marketing">Marketing</option>
										<option value="Developpement Web">Développeur Web</option>
										<option value="Charger">Superviseur</option>
									</select>
								</div>
		            		</div>
		            		<div class="col-sm-6">
			            		<div class="form-group">
									<label for="sexe">Sexe </label>
									<br>
								      <input type="radio" name="sexe" value="Masculin"> Masculin
								      <input type="radio" name="sexe" value="Feminin">  Féminin
								</div>
							</div>
		            	</div>
					</div>
	          	</div>
	        </div>
	     </div>
	     <p class="change_link"></p>
	     <div id="kv-avatar-errors-1" class="center-block" style="width:800px;display:none"></div>
		<input type="button" class="next-form btn btn-info" value="Suivant" />
		</fieldset>
	<fieldset>

	<h2> Etape 2</h2>
	 <div class="row">
	    	<div class="col-sm-6">
				<div class="form-group">
					<label for="email">Addresse Email *</label>
					<input type="email" class="form-control input2" required id="email" onblur="verifMail(this)" name="email" placeholder="Email" required>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label for="portable">Portable*</label>
					<input type="text" class="form-control input2" name="portable" id="portable" onblur="verifPhone(this)" placeholder="Numéro de Téléphone 70/00/01/02" required>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label for="address">Addresse <a class="btn btn-info" data-toggle="popover-x" data-target="#myPopover2" data-placement="right">aide</a>
				 	<!-- PopoverX content -->
			        <div id="myPopover2" class="popover popover-default">
			            <div class="arrow"></div>
			            <h3 class="popover-title"><span class="close pull-right" data-dismiss="popover-x">&times;</span>Comment Décrire Votre Addresse</h3>
			            <div class="popover-content">
			                <p>Veuillez nous décrire brièvement votre addresse avec les grands points de votre ville. Par exemple Je suis au 1200 logement. Arrivé à au feu de Marina Market en venant de la pédiatrie, tourner à gauche et rentrez dans le 8ème six-mètre à droite, 2ème porte à gauche.  </p>
			            </div>
			        </div>
			    	</label>
					<textarea  class="form-control input2" name="addresse" id="addresse" onblur="verifAddress(this)" placeholder="votre Addresse(veuillez nous fournir une brève indication juska votre domicile!!!)" cols="5" rows="8" required></textarea>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label for="numcnib">Numéro de la CINB ou du Passeport</label>
					<input type="text" class="form-control input2" name="numcnib" id="numcnib" onblur="verifCNIB(this)" placeholder="Numéro de la CINB ou du Passeport" required>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label for="diplome">Dernier Diplôme</label>
					<input type="text" class="form-control input2" name="diplome" id="diplome"  onblur="verifInput(this)" placeholder="Dernier Diplôme" required>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label for="universite">Dans quelle Université?</label>
					<input type="text" class="form-control input2" name="universite" id="universite" onblur="verifInput(this)"  placeholder="Dans quelle Université avez vous reçu votre Dernier Diplôme?" required>
				</div>
			</div>
		</div>
		<p class="change_link"></p>
		<input type="button" name="previous" class="previous-form btn btn-default" value="Précédent" />
		<input type="button" name="next" class="next-form btn btn-info" value="Suivant" />
	</fieldset>

	<fieldset>
	<h2>Etape 3</h2>
	<div class="row">
			<div class="col-sm-12">
                <div class="form-group">
                    <label class="control-label">Veuillez Soumettre Votre Dernier Diplome Ou Attestation, Votre CNIB et Une Photo Récente de Vous (Moins de 03 mois). Selection Multiple Ok</label>
                    <input type="hidden" name="nomImages" id="nomImages" value="">
                    <input id="images" name="images[]" type="file" multiple>
                    <br>
	                <script>
		                $(document).on("ready", function() {
						    $("#images").fileinput({
						    	language: 'fr',
						        uploadAsync: false,
						        browseOnZoneClick: true,
						        uploadUrl: "https://yanfoma.tech/contrat/upload.php"
						    }).on('filebatchpreupload', function(event, data) {
								var inp = document.getElementById('images');
								var stringName="";
								for (var i = 0; i < inp.files.length; ++i) {
								  	var name = inp.files.item(i).name;
								  	stringName += name + " * ";
								}
								document.getElementById('nomImages').value = stringName;
							});
						});
	                </script>
                </div>
			</div>
		</div>
		<p class="change_link"></p>
		<input type="button" name="previous" class="previous-form btn btn-default" value="Précédent" />
		<input type="submit" name="submit"   class="submit btn btn-success" value="Soumettre" />
	</fieldset>
	</form>
</div></div>
</body>
<script type="text/javascript">

function surligne(champ, erreur)
{
   if(erreur)
      champ.style.backgroundColor = "#ffb5b5";
   else
      champ.style.backgroundColor = "#84e0b5";
}

function verifInput(champ)
{
   if(champ.value.length < 2 || champ.value.length > 35)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}
function verifAddress(champ)
{
   if(champ.value.length < 0 || champ.value.length < 25)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}


function verifMail(champ)
{
   var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
   if(!regex.test(champ.value))
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}
function verifAge(champ)
{
   var age = parseInt(champ.value);
   if(isNaN(age) || age < 20 || age > 111)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}
function verifCNIB(champ)
{
   var cnib = parseInt(champ.value);
   if(isNaN(cnib) || cnib.toString().length !=17)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;

   }
}
function verifPhone(champ)
{
   //var regex =/^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g
   var phone = parseInt(champ.value);

   if(phone < 1 || age > 18)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}

function verifForm(f)
{
   var phoneOk   				= verifPhone(f.pseudo);
   var nomOK       				= verifInput(f.nom);
   var prenomsOK    			= verifInput(f.prenom);
   var dateOfBirthOK      		= verifInput(f.dateOfBirth);
   var placeOfBirthOK    		= verifInput(f.placeOfBirth);
   var quartierOK   		    = verifInput(f.quartier);
   var ageOK    				= verifAge(f.age);
   var emailOK     				= verifMail(f.email);
   var diplomeOK 				= verifInput(f.diplome);
   var universiteOK				= verifInput(f.universite);
   var numcnibOK			  	= verifCNIB(f.numcnib);


   if(phoneOk && nomOK && prenomsOK && dateOfBirthOK && placeOfBirthOK && quartierOK && ageOK && emailOK && diplomeOK && niversiteOK && umcnibOK)
      return true;
   else
   {
      alert("Veuillez remplir correctement tous les champs");
      return false;
   }
}

var format = "mm/dd/yyyy";
var match = new RegExp(format
    .replace(/(\w+)\W(\w+)\W(\w+)/, "^\\s*($1)\\W*($2)?\\W*($3)?([0-9]*).*")
    .replace(/m|d|y/g, "\\d"));
var replace = "$1/$2/$3$4"
    .replace(/\//g, format.match(/\W/));

function doFormat(target)
{
    target.value = target.value
        .replace(/(^|\W)(?=\d\W)/g, "$10")   // padding
        .replace(match, replace)             // fields
        .replace(/(\W)+/g, "$1");            // remove repeats
}

$("input[name='dateOfBirth']:first").keyup(function(e) {
   if(!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
      doFormat(e.target)
});

//Put our input DOM element into a jQuery Object
var $jqDate = jQuery('input[name="portable"]');

//Bind keyup/keydown to the input
$jqDate.bind('keyup','keydown', function(e){

  //To accomdate for backspacing, we detect which key was pressed - if backspace, do nothing:
    if(e.which !== 8) {
        var numChars = $jqDate.val().length;
        if(numChars === 1){
            var thisVal = $jqDate.val();
            thisVal = '(+226) ' + thisVal;
            $jqDate.val(thisVal);
        }
        if(numChars === 9 || numChars === 12 || numChars === 15){
            var thisVal = $jqDate.val();
            thisVal += '/';
            $jqDate.val(thisVal);
        }
  }
});


//the fileinput plugin initialization
/*var btnCust = '';
$("#avatar-1").fileinput({
    overwriteInitial: true,
    maxFileSize: 5000000,
    showClose: true,
    showCaption: false,
    language: 'fr',
    uploadUrl: "http://localhost/contrat/upload.php",
    browseLabel: 'choisir',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Supprimer or réinitialiser',
    elErrorContainer: '#kv-avatar-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="uploads/default_avatar_male.jpg" alt="Votre  Photo" style="width:160px">',
    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
    allowedFileExtensions: ["jpg", "png", "gif"]
});


   /* $('#file-fr').fileinput({
        language: 'fr',
        uploadAsync: false,
        uploadUrl: 'http://localhost/contrat/upload.php',
        allowedFileExtensions: ['jpg', 'png', 'gif']
    });

    $("#diplome").fileinput({
    uploadUrl: "http://localhost/contrat/uploads/diplomes",
    uploadAsync: true,
    language: 'fr',
    minFileCount: 2,
    maxFileCount: 5,
    overwriteInitial: false,
    initialPreviewAsData: false // identify if you are sending preview data only and not the raw markup
	});

    $(".btn-warning").on('click', function () {
        var $el = $("#file-4");
        if ($el.attr('disabled')) {
            $el.fileinput('enable');
        } else {
            $el.fileinput('disable');
        }
    });
    $(".btn-info").on('click', function () {
        $("#file-4").fileinput('refresh', {previewClass: 'bg-info'});
    });

    $(document).ready(function () {
    $("#cnib").fileinput({
    	language: 'fr',
	    //showPreview: false,
	    allowedFileExtensions: ["jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "pdf"],
	    // you can configure `msgErrorClass` and `msgInvalidFileExtension` as well
	    //AJAX
	        uploadAsync: false,
	        uploadUrl: "http://localhost/contrat/upload.php", // your upload server url
	        uploadExtraData: function() {
	            return {
	                server: $("input[name='nom']").val(),
	                user:   $("input[name='prenoms']").val()
	            };
	        }
	     });
        /*$("#kv-explorer").fileinput({
            'theme': 'explorer',
            'uploadUrl': '#',
            overwriteInitial: false,
            initialPreviewAsData: true,
            initialPreview: [
                "http://lorempixel.com/1920/1080/nature/1",
                "http://lorempixel.com/1920/1080/nature/2",
                "http://lorempixel.com/1920/1080/nature/3"
            ],
            initialPreviewConfig: [
                {caption: "nature-1.jpg", size: 329892, width: "120px", url: "{$url}", key: 1},
                {caption: "nature-2.jpg", size: 872378, width: "120px", url: "{$url}", key: 2},
                {caption: "nature-3.jpg", size: 632762, width: "120px", url: "{$url}", key: 3}
            ]
        });*/
        /*
         $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
         alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
         });

    });*/
</script>
</html>