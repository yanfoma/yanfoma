<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<script src="js/modernizr.js"></script> <!-- Modernizr -->

	<title>Contrat De Stage à Yanfoma </title>
</head>
<body>
	<img class="logo" src="img/logo.png">
	<h1 class="contenth1">Contrat De Stage à Yanfoma</h1>

	<main class="cd-main-content">
		<section class="center">
			<p style="line-height: 200%; text-align:justify;  text-indent: 50px;">
				Tout d'abord nous voudrons porter à votre connaissance les termes du contrat de stage à Yanfoma. Après prise de connaissance des termes nous vous inviterons à passer à la prochaine étape
				qui consiste à la signature et pour cela elle se fera en cliquant sur la case portant la mention<b style="font-weight:  900;"> &#171;lue et apprové&#187;.</b>
				 Ensuite vous serez dirigé sur une autre page pour remplir vos données et le contrat sera généré automatiquement. Veuillez verifier vos données avant soumission.
				Cela fait, le contrat de stage sera téléchargé directement. Nous prions donc de remplir ce contrat en utilisant un ordinateur. Merci<br>
				Nous sommes impatients de travailler avec vous!!!!!
			</p><br>
			<center><p style="text-decoration:underline;">Cordialement L'équipe Yanfoma</p></center><br><br>
			<a href="#0" class="cd-btn center" id="modal-trigger" data-type="cd-modal-trigger">Voir Le contrat </a>
		</section>
	</main> <!-- .cd-main-content -->

	<div class="cd-modal" data-modal="modal-trigger">
		<div class="cd-svg-bg"
		data-step1="M-59.9,540.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L864.8-41c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3L-59.5,540.6 C-59.6,540.7-59.8,540.7-59.9,540.5z"
		data-step2="M33.8,690l-188.2-300.3c-0.1-0.1,0-0.3,0.1-0.3l925.4-579.8c0.1-0.1,0.3,0,0.3,0.1L959.6,110c0.1,0.1,0,0.3-0.1,0.3 L34.1,690.1C34,690.2,33.9,690.1,33.8,690z"
		data-step3="M-465.1,287.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L459.5-294c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3 l-925.4,579.8C-464.9,287.7-465,287.7-465.1,287.5z"
		data-step4="M-329.3,504.3l-272.5-435c-0.1-0.1,0-0.3,0.1-0.3l925.4-579.8c0.1-0.1,0.3,0,0.3,0.1l272.5,435c0.1,0.1,0,0.3-0.1,0.3 l-925.4,579.8C-329,504.5-329.2,504.5-329.3,504.3z"
		data-step5="M341.1,797.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L1265.8,216c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3L341.5,797.6 C341.4,797.7,341.2,797.7,341.1,797.5z"
		data-step6="M476.4,1013.4L205,580.3c-0.1-0.1,0-0.3,0.1-0.3L1130.5,0.2c0.1-0.1,0.3,0,0.3,0.1l271.4,433.1c0.1,0.1,0,0.3-0.1,0.3 l-925.4,579.8C476.6,1013.6,476.5,1013.5,476.4,1013.4z">
			<svg height="100%" width="100%" preserveAspectRatio="none" viewBox="0 0 800 500">
				<title>SVG Modal background</title>
				<path id="cd-changing-path-1" d="M-59.9,540.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L864.8-41c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3L-59.5,540.6 C-59.6,540.7-59.8,540.7-59.9,540.5z"/>
				<path id="cd-changing-path-2" d="M-465.1,287.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L459.5-294c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3 l-925.4,579.8C-464.9,287.7-465,287.7-465.1,287.5z"/>
				<path id="cd-changing-path-3" d="M341.1,797.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L1265.8,216c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3L341.5,797.6 C341.4,797.7,341.2,797.7,341.1,797.5z"/>
			</svg>
		</div>

		<div class="cd-modal-content" style="text-align: justify;">
			<p>Contrat de Stage A Yanfoma</p>
			<p>
				ENTRE LES SOUSSIGNES:
				<b style="font-weight:  900;">Yanfoma Ltd</b> Société à responsabilitée Limitée (en anglais Limited) sise à Taiwan.
				<br >Tel:(+886) 989-597-235 / (+886) 903-550-202. Site web:yanfoma.tech.
				<br >Représenté son Directeur Général <b style="font-weight:  900;">Monsieur Amon Moce Bazongo.</b>
				<br >ci-après désignée &#171;l'employeur&#187;.
				<br >D'une part,
				<br >Et
				<br >Monsieur Smith John
				<br >Date et Lieu de Naissance: 01/01/1900 à Ouagadougou
				<br >Domicilié à 1200 Logements
				<br >Nationalité: Burkinabé
				<br >Situation Matrimoniale: Celibataire Sans Enfant
				<br >Tel: 70707070
				<br >Email: johnsmith@yahoo.fr
				<br >Numéro CNIB: 123456789
				<br >Dernier Diplome obtenu : Master en Marketing à ISGE-BF
				<br >D'autre part,
				<br >Il a été préalablement exposé ce qui suit:
			</p>
			<p>
				Monsieur Smith John a introduit la demande de stage auprès de Yanfoma qui a marqué son accord. Afin d'assurer le bon déroulement de celui-ci les parties ont convenu et arrêté ce qui suit:
			</p>

			<p>
				<b style="font-weight:900;text-decoration:underline;">Article 1: Modalités</b>
				<br>Le présent contrat a pour objet le stage en Marketing à éffectuer à yanfoma par Smith John prévu pour (02) mois qui se déroulera du
				12 Juillet 2017 au 12 Septembre 2017 inclus.  Le présent contrat est porté à la connaissance du stagiaire qui doit donner son consentement en cliquant sur la case portant la mention &#171; lu et apprové&#187;. En cas d'empèchement il doit d'avertir Yanfoma dans un délai de deux (02) Jours. La politesse et la courtoisie sont de rigueur pour les deux parties. Le stagiaire est tenu de respecter la confidentialité des données ou informations qui lui seront confiées ou qu'il découvrira lors de son stage.

			</p>
			<p>
				<b style="font-weight:900;text-decoration:underline;">Article 2: Tâches</b>
				<br>
				Yanfoma s'engage à définir clairement au stagiaire l'étendue des taches à accomplir par ce dernier. Elles pourront lui être données au fur et à mésure de leur exécution.
			</p>
			<p>
				<b style="font-weight:900;text-decoration:underline;">Article 3: Obligations du Stagiaire</b>
				<br>
				Le stagiaire a l'obligation d'éffectuer sérieusement et consciencieusement les tâches qui lui sont assignées. Le délai de reporting est de rigueur. En cas de manquement à ces obligations, Yanfoma par son représentant se réserve le droit de mettre fin au stage sans indemnité ni préavis.

			</p>
			<p>
				<b style="font-weight:900;text-decoration:underline;">Article 4: Rémunération</b>
				<br>Le stagiaire percevra une prime de stage mensuelle dont la moitié sera versée à la fin de chaque mois par le biais de Moneygram. Le montant de la prime sera fonction du volume de travail et des responsabilités attribuées au stagiaire. Yanfoma se réserve le droit de le déterminer unilatéralement. Le stagiaire ne bénéficie d'aucune protection sociale.

			</p>
			<form action="contrat.php" onsubmit="if(document.getElementById('agree').checked) { return true; } else 	{ alert('Veuillez cocher la case  &#171;lue et apprové&#187; puis continuez.'); return false; }">

			    <input type="checkbox" class="checkbox" name="checkbox" value="check" id="agree"  />
			    <label class="label--checkbox">&#171;lue et apprové&#187;</label>
			    <input class="cd-btn" type="submit" name="approuve" value="Continuer" />
			</form>
		</div> <!-- cd-modal-content -->

		<a href="#0" class="modal-close">Close</a>
	</div> <!-- cd-modal -->

	<div class="cd-cover-layer"></div> <!-- .cd-cover-layer -->
<script src="js/jquery_2.1.4.js"></script>
<script src="js/snap.svg-min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
</body>
</html>