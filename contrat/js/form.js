$(document).ready(function(){
  var form_count = 1, previous_form, next_form, total_forms;
  total_forms = $("fieldset").length;
  $(".next-form").click(function(){
    previous_form = $(this).parent();
    next_form = $(this).parent().next();
    next_form.show();
    previous_form.hide();
    setProgressBarValue(++form_count);
  });
  $(".previous-form").click(function(){
    previous_form = $(this).parent();
    next_form = $(this).parent().prev();
    next_form.show();
    previous_form.hide();
    setProgressBarValue(--form_count);
  });
  setProgressBarValue(form_count);
  function setProgressBarValue(value){
    var percent = parseFloat(100 / total_forms) * value;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width",percent+"%")
      .html(percent+"%");
  }
  // Handle form submit and validation
  $( "#register_form" ).submit(function(event) {
	var error_message = '';
	if(!$("#email").val()) {
		error_message+="Veuillez renseigner votre email";
	}
  if(!$("#nom").val()) {
    error_message+="<br>Veuillez renseigner votre nom";
  }
  if(!$("#prenoms").val()) {
    error_message+="<br>Veuillez renseigner vos prénoms";
  }
  if(!$("#dateOfBirth").val()) {
    error_message+="<br>Veuillez renseigner votre date de naissance";
  }
  if(!$("#placeOfBirth").val()) {
    error_message+="<br>Veuillez renseigner votre lieu de naissance";
  }
  if(!$("#quartier").val()) {
    error_message+="<br>Veuillez renseigner votre quartier";
  }
  if(!$("#addresse").val()) {
    error_message+="<br>Veuillez renseigner votre addresse";
  }
  if(!$("#age").val()) {
    error_message+="<br>Veuillez renseigner votre age";
  }
  if(!$("#nationalite").val()) {
    error_message+="<br>Veuillez renseigner votre nationalite";
  }
  if(!$("#portable").val()) {
    error_message+="<br>Veuillez renseigner votre portable";
  }
  if(!$("#diplome").val()) {
    error_message+="<br>Veuillez renseigner votre diplome";
  }
  if(!$("#universite").val()) {
    error_message+="<br>Veuillez renseigner votre universite";
  }
  if(!$("#numcnib").val()) {
    error_message+="<br>Veuillez renseigner votre Numéro CNIB";
  }
	// Display error if any else submit form
	if(error_message) {
		$('.alert-danger').removeClass('hide').html(error_message);
		return false;
	} else {
		return true;
	}
  });

});