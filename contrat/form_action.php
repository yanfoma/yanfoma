<?php
require('fpdf/fpdf.php');
include_once("db_connect.php");
class PDF extends FPDF{

	function Cell($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='') {
		parent::Cell($w,$h, utf8_decode($txt), $border,$ln,$align,$fill,$link);
	}

	function Header(){
	    // Logo
	    $this->Image('logo.png',10,0,30);
	    // Arial bold 15
		global $title;
		// Arial bold 15
		$this->SetFont('Times','B',15);
		// Calculate width of title and position
		$w = $this->GetStringWidth($title)+6;
		$this->SetX((210-$w)/2);
		// Colors of frame, background and text
		$this->SetTextColor(FF,FF,FF);
	}

	function Footer(){

		if($this->PageNo()==1)
	    {
	        //First page
		    // Signature
	         $this->Image('img/signature.png',-10,255,100);
	          // Position at 1.5 cm from bottom
			$this->SetY(-15);
	        //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	        $this->Cell(0,10,'Page '.$this->PageNo().'/ {nb}',0,0,'C');

	    }
	    else
	    {
	        //Other pages
	        // Position at 1.5 cm from bottom
			$this->SetY(-15);
			// Arial italic 8
			$this->SetFont('Arial','I',8);
			// Text color in gray
			$this->SetTextColor(128);
			// Page number
			$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	    }
	}
}

function diverse_array($vector) {
        $result = array();
        foreach($vector as $key1 => $value1){
            foreach($value1 as $key2 => $value2){
                $result[$key2][$key1] = $value2;
            }
        }
        return $result;
    }

	if (isset($_POST['submit'])) {
		$civilite 			= mysqli_real_escape_string($conn, $_POST['civilite']);
		$nationalite		= mysqli_real_escape_string($conn, $_POST['nationalite']);
		$statut 			= mysqli_real_escape_string($conn, $_POST['statut']);
		$nom 				= mysqli_real_escape_string($conn, $_POST['nom']);
		$prenoms 			= mysqli_real_escape_string($conn, $_POST['prenoms']);
		$dateOfBirth 		= mysqli_real_escape_string($conn, $_POST['dateOfBirth']);
		$placeOfBirth 		= mysqli_real_escape_string($conn, $_POST['placeOfBirth']);
		$domicile 			= mysqli_real_escape_string($conn, $_POST['domicile']);
		$quartier 			= mysqli_real_escape_string($conn, $_POST['quartier']);
		$address 			= mysqli_real_escape_string($conn, $_POST['addresse']);
		$age 				= mysqli_real_escape_string($conn, $_POST['age']);
		$sexe 				= mysqli_real_escape_string($conn, $_POST['sexe']);
		$email 				= mysqli_real_escape_string($conn, $_POST['email']);
		$portable 			= mysqli_real_escape_string($conn, $_POST['portable']);
		$diplome 			= mysqli_real_escape_string($conn, $_POST['diplome']);
		$universite 		= mysqli_real_escape_string($conn, $_POST['universite']);
		$numcnib 			= mysqli_real_escape_string($conn, $_POST['numcnib']);
	    $position   		= mysqli_real_escape_string($conn, $_POST['position']);
	    $nomImages   		= mysqli_real_escape_string($conn, $_POST['nomImages']);
	    //echo "le nom des images est $nomImages";
		$d      = getDate();
        $date   = "$d[month] $d[mday] $d[year]";
        $query  = "INSERT INTO stagiaires(civilite,statut,nom,prenoms,dateOfBirth,placeOfBirth,domicile,quartier,address,age,sexe,email,portable,diplome,universite,numcnib,images,nationalite,date)
		                       VALUES('$civilite','$statut','$nom','$prenoms','$dateOfBirth','$placeOfBirth','$domicile','$quartier','$address','$age','$sexe','$email','$portable','$diplome','$universite','$numcnib','$nomImages','$nationalite','$date')";

		if($conn->query($query) === TRUE)
		{
			$pdf = new PDF('P','mm','A4');
			$pdf->AddPage();
			$title ='Contrat de Stage A Yanfoma';
			// Calculate width of title and position
		    $w = $pdf->GetStringWidth($title)+6;
		    $pdf->SetX((210-$w)/2);
		    // Colors of frame, background and text
		    $pdf->SetDrawColor(0,0,0);
		    $pdf->SetFillColor(240,240,240);
		    $pdf->SetTextColor(0,0,0);
		    // Thickness of frame (1 mm)
		    $pdf->SetLineWidth(1);
		    $pdf->Cell($w,10,$title,1,1,'C',true);
			$pdf->SetFont('Times','BU',8);
			// Move to 8 cm to the right
			//$pdf->Cell(18);
			$pdf->Ln(10);
			$pdf->Write(5,'ENTRE LES SOUSSIGNES:');
			$pdf->Ln(5);
			$pdf->SetFont('Times','B',9);
			$pdf->Write(5,'Yanfoma Ltd ');
			$pdf->SetFont('Times','',9);
			$pdf->Write(5,"Société à responsabilitée Limitée (en anglais Limited) sise à Taiwan.
Tel:(+886) 989-597-235 / (+886) 903-550-202. Site web:yanfoma.tech.
Représenté par son Directeur Général Monsieur Amon Moce Bazongo.
ci-après désignée <<l'employeur>>.
D'une part,
Et
$civilite $nom $prenoms
Date et Lieu de Naissance: $dateOfBirth à $placeOfBirth
Domicilié à $quartier
Nationalité: $nationalite
Situation Matrimoniale: $statut
Tel: $portable
Email: $email
Numéro CNIB: $numcnib
Dernier Diplome obtenu : $diplome à $universite
D'autre part,
Il a été préalablement exposé ce qui suit:

$civilité $nom $prenoms a introduit la demande de stage auprès de Yanfoma qui a marqué son accord. Afin d'assurer le bon déroulement de celui-ci les parties ont convenu et arrêté ce qui suit:");
			$pdf->Ln(10);
			$pdf->SetFont('Times','BU',10);
			$pdf->SetTextColor(0,0,0);
			$pdf->Write(5,'Article 1: Modalités');
			$pdf->SetFont('Times','',9);
			$pdf->Write(5,"
Le présent contrat a pour objet le stage en $position à éffectuer à yanfoma par $civilité $nom $prenoms prévu pour (02) mois qui se déroulera du 12 Juillet 2017 au 12 Septembre 2017. Le présent contrat est porté à la connaissance du stagiaire qui a donner son consentement en cliquant sur la case portant la mention << lu et approuvée>>. En cas d'empèchement il doit d'avertir Yanfoma dans un délai de deux (02) Jours. La politesse et la courtoisie sont de rigueur pour les deux parties. Le stagiaire est tenu de respecter la confidentialité des données ou informations qui lui seront confiées ou qu'il découvrira lors de son stage.");

			$pdf->Ln(10);
			$pdf->SetFont('Times','BU',10);
			$pdf->Write(5,'Article 2: Tâches');
			$pdf->Ln(1);
			$pdf->SetFont('Times','',9);
			$pdf->Write(5,"
Yanfoma s'engage à définir clairement au stagiaire l'étendue des taches à accomplir par ce dernier. Elles pourront lui être données au fur et à mésure de leur exécution.");
			$pdf->Ln(10);
			$pdf->SetFont('Times','BU',10);
			$pdf->Write(5,'Article 3: Obligations du Stagiaire');
			$pdf->Ln(1);
			$pdf->SetFont('Times','',9);
			$pdf->Write(5,"
Le stagiaire a l'obligation d'éffectuer sérieusement et consciencieusement les tâches qui lui sont assignées. Le délai de reporting est de rigueur. En cas de manquement à ces obligations, Yanfoma par son représentant se réserve le droit de mettre fin au stage sans indemnité ni préavis.");
			$pdf->Ln(10);
			$pdf->SetFont('Times','BU',10);
			$pdf->Write(5,'Article 4: Rémunération');
			$pdf->Ln(1);
			$pdf->SetFont('Times','',9);
			$pdf->Write(5,"
Le stagiaire percevra une prime de stage mensuelle dont la moitié sera versée à la fin de chaque mois par le biais de Moneygram. Le montant de la prime sera fonction du volume de travail et des responsabilités attribuées au stagiaire. Yanfoma se réserve le droit de le déterminer unilatéralement. Le stagiaire ne bénéficie d'aucune protection sociale.");
			$pdf->Ln(7);
			$pdf->Write(5,"
Fait à Taipei, le 11/Juillet/2017.");
			$pdf->SetFont('Times','BU',10);
			$pdf->SetAuthor('Yanfoma');
			$pdf->Ln(7);
			$pdf->SetFont('Times','',10);
			$pdf->Write(5,'Amon Moce Rodolphe Bazongo');
			$pdf->Ln(5);
			$pdf->SetFont('Times','I',10);
			$pdf->Write(5,'Directeur de Yanfoma');
			$pdf->AliasNbPages();
			$pdf->Output(D,"Contrat de Stage de $nom $prenoms.pdf");
			$pdf->Output('I');
			$pdf->Output(F,"contrats/Contrat de Stage de $nom $prenoms.pdf");

			//$filename ="$nom$prenoms.pdf";
			//$path = "contrats/".$filename;

			//$doc = $pdf->Output('S');
			//echo $filename;
			//$pdf_attach= $pdf->Output('contratDeStage.pdf','I');
	}
}

	?>