<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>YanfoWeb || Advertisement Videos And Posters</title>
    <?php include_once("librairies/meta.php"); ?>
    <link rel="stylesheet" type="text/css" href="js/video/content/global.css">
</head>
<body>

<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<section class="service style-2 sec-padd">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="outer-box">
                    <div class="intro-img">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="growth-item center">
                                <div class="icon_box">
                                    <span class="icon-business"></span>
                                </div>
                                <a href="#"><h4>Web Plan</h4></a>
                                <div class="content center">
                                    <p>Which of ever undertke laborious <br>physical exercised excepts.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="growth-item center">
                                <div class="icon_box">
                                    <span class="icon-graphic"></span>
                                </div>
                                <a href="#"><h4>Cloud & Social Networks Applications</h4></a>
                                <div class="content center">
                                    <p>Great explorer of the truth, the <br>master-builder of human happiness</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="growth-item center">
                                <div class="icon_box">
                                    <span class="icon-business-1"></span>
                                </div>
                                <a href="#"><h4>Community Management</h4></a>
                                <div class="content center">
                                    <p>chooses to enjoy a pleasure that  <br>has no annoing consequences,</p>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <div class="section-title">
                        <h3>Web Package</h3>
                    </div>
                    <div class="text">
                        <p>Employers across all industry sectors to ensure that their internal sed Human Resource systems processes align to their business requirements idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth.</p>
                    </div>
                    <div class="tabs-outer">
                        <!--Tabs Box-->
                        <div class="tabs-box tabs-style-two">
                            <!--Tab Buttons-->
                            <ul class="tab-buttons clearfix">
                                <li data-tab="#tab-one"   class="tab-btn active-btn">Web Site</li>
                                <li data-tab="#tab-two"   class="tab-btn">Web Hosting </li>
                                <li data-tab="#tab-three" class="tab-btn">SEO</li>
                            </ul>

                            <!--Tabs Content-->
                            <div class="tabs-content">
                                <!--Tab / Active Tab-->
                                <div class="tab active-tab" id="tab-one" style="display: block;">
                                    <div class="text-content clearfix">
                                        <div class="img-box float_left">
                                            <a href="#"><img src="images/service/site.png" alt=""></a>
                                        </div>
                                        <div class="text float_left"><p>Righteous indignation and dislike men who are so beguiled demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and those who fail in their duty through weakness.</p><p>Beguiled demoralized by the charms of pleasure of seds the moment, so blinded by desire, that they cannot foresee the pain and trouble.</p></div>
                                    </div>
                                </div>

                                <!--Tab-->
                                <div class="tab" id="tab-two" style="display: none;">
                                    <div class="text-content clearfix">
                                        <div class="img-box float_left">
                                            <a href="#"><img src="images/service/hosting.png" alt=""></a>
                                        </div>
                                        <div class="text float_left"><p>Righteous indignation and dislike men who are so beguiled demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and those who fail in their duty through weakness.</p><p>Beguiled demoralized by the charms of pleasure of seds the moment, so blinded by desire, that they cannot foresee the pain and trouble.</p></div>
                                    </div>
                                </div>

                                <!--Tab-->
                                <div class="tab" id="tab-three" style="display: none;">
                                    <div class="text-content clearfix">
                                        <div class="img-box float_left">
                                            <a href="#"><img src="images/service/seo.png" alt=""></a>
                                        </div>
                                        <div class="text float_left"><p>Righteous indignation and dislike men who are so beguiled demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and those who fail in their duty through weakness.</p><p>Beguiled demoralized by the charms of pleasure of seds the moment, so blinded by desire, that they cannot foresee the pain and trouble.</p></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br><br><br>
                </div>
            </div>

        </div>
    </div>
</section>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
</div>

</body>
</html>