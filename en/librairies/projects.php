<section class="latest-project sec-padd">
    <div class="container">
        <div class="section-title">
            <h2>Latest Projects</h2>
        </div>
        <div class="latest-project-carousel">
            <!--div class="item">
                <div class="single-project">
                    <figure class="imghvr-shutter-in-out-horiz">
                        <img src="images/resource/caya.png" alt="Caya Yanfoma Product"/>
                        <figcaption>
                            <div class="content">
                                <a href="http://caya.tw"><h4>view</h4></a>
                                <p>Caya</p>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div-->
            <div class="item">
                <div class="single-latest-project-carousel">
                    <div class="single-project">
                    <figure class="imghvr-shutter-in-out-horiz">
                        <img src="images/resource/warifaprojet.png" alt="Warifa Yanfoma Product "/>
                        <figcaption>
                            <div class="content">
                                <a href="#"><h4>Coming Soon</h4></a>
                                <p>Warifa</p>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                </div><!-- /.single-latest-project-carousel -->
            </div>
            <div class="item">
                <div class="single-latest-project-carousel">
                    <div class="single-project">
                    <figure class="imghvr-shutter-in-out-horiz">
                        <img src="images/resource/smartRouko.png" alt="SmartRouko Yanfoma Product "/>
                        <figcaption>
                            <div class="content">
                                <a href="#"><h4>Coming Soon</h4></a>
                                <p>Smart Rouko</p>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                </div>
            </div>
            <div class="item">
                <div class="single-latest-project-carousel">
                    <div class="single-project">
                    <figure class="imghvr-shutter-in-out-horiz">
                        <img src="images/resource/kaanu.png" alt="kaanu Yanfoma Product"/>
                        <figcaption>
                            <div class="content">
                                <a href="http://kaanu-center.herokuapp.com"><h4>Contribute</h4></a>
                                <p>Kaanu</p>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                </div>
            </div>
        </div>

    </div>
</section>