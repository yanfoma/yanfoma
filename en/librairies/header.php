<?php include_once('function.php');?>
<header class="top-bar">
    <div class="container">
        <div class="clearfix">
            <div class="col-left float_left">
                <div id="polyglotLanguageSwitcher" class="polyglotLanguageSwitcher">
                    <form action="#">
                        <select id="polyglot-language-options">
                            <option id="en" value="en" selected>English</option>
                            <option id="fr" value="fr">Français</option>
                        </select>
                    </form>
                </div>
                <ul class="top-bar-info">
                  <?php if(basename($_SERVER['PHP_SELF']) == "index.php"){?>
                      <li><a href="https://yanfoma.tech/en/contact.php" class="quotation">Get a quote and a discount now</a></li>
                    <?php }else{ ?>
                      <li><i class="icon-technology"></i>Phone: (+886) 9895 97235</li>
                      <li><i class="icon-note2"></i>info@yanfoma.tech</li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-right float_right">
                <ul class="social">
                    <li>Stay Connected: </li>
                    <li><a href="https://www.facebook.com/yanfoma/"                             target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCXm7arljVJbdFIIEVdnt-LQ"      target="_blank"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="https://plus.google.com/u/0/100764898996932239647"             target="_blank"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/yanfoma-tech-958054145/"           target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</header>

<section class="theme_menu stricky">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="main-logo">
                    <a href="index.php"><img src="images/logo/Yanfoma-logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-8 menu-column">
                <nav class="menuzord" id="main_menu">
                   <ul class="menuzord-menu">
                        <li class="<?php active(index);?>"    ><a href="index.php">Home</a></li>
                        <li class="<?php active(about);?>"    ><a href="about.php">About us</a></li>
                        <li class="" ><a href="#">services</a>
                            <ul class="dropdown">
                                <!--li class="<?php //active(services);?>">  <a href="services.php">View All </a></li>
                                <li class="<?php// active(media);?>">     <a href="web.php">YanfoWeb </a></li-->
                                <li class="<?php active(media);?>">     <a href="media.php">YanfoMedias </a></li>
                            </ul>
                        </li>
                        <!--li class="#"><a href="#">Blog</a>
                            <ul class="dropdown">
                                <li class="<?php //active(blog);?>">  <a href="blog.php">YanfoBlog</a></li>
                                <li class="<?php //active(career);?>"><a href="career.php">Carrer </a></li>
                                <li class="<?php //active(faq);?>">   <a href="#">Education </a></li>
                            </ul>
                        </li-->
                        <!--li class="<?php //active(store);?>"  >   <a href="#">Store</a></li-->
                        <li class="<?php active(contact);?>"  > <a href="contact.php">Contact us</a></li>
                    </ul><!-- End of .menuzord-menu -->
                </nav> <!-- End of #main_menu -->
            </div>
        </div>


   </div> <!-- End of .conatiner -->
</section>
