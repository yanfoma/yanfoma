<body>
		<!-- div used as a holder for the coverflow -->
		<div id="myDiv2"></div>

		<!-- coverflow data -->
		<ul id="coverflowData" style="display: none;">

			<!-- category  -->
			<ul data-cat="+ Category two">
				<ul>
					<li data-url="js/image/load/media/images/kaanu.jpg"></li>
					<li data-thumbnail-path="js/image/load/media/images/kaanu.jpg"></li>
					<li data-thumbnail-text=""></li>
					<li data-info=""></li>
				</ul>

				<ul>
					<li data-url="js/image/load/media/images/caya.jpg"></li>
					<li data-thumbnail-path="js/image/load/media/images/caya.jpg"></li>
					<li data-thumbnail-text=""></li>
					<li data-info=""></li>
				</ul>
				<ul>
					<li data-url="js/image/load/media/images/warifa.jpg"></li>
					<li data-thumbnail-path="js/image/load/media/images/warifa.jpg"></li>
					<li data-thumbnail-text=""></li>
					<li data-info=""></li>
				</ul>
				<ul>
					<li data-url="js/image/load/media/images/satnet.jpg"></li>
					<li data-thumbnail-path="js/image/load/media/images/satnet.jpg"></li>
					<li data-thumbnail-text=""></li>
					<li data-info=""></li>
				</ul>
				<ul>
					<li data-url="js/image/load/media/images/festiba.jpg"></li>
					<li data-thumbnail-path="js/image/load/media/images/festiba.jpg"></li>
					<li data-thumbnail-text=""></li>
					<li data-info=""></li>
				</ul>

			</ul>
			<!-- end  -->
		</ul>

	</body>

		<!-- Imports required for the css and js files -->
		<link rel="stylesheet" type="text/css" href="js/image/load/skin_classic_black.css"/>
		<script type="text/javascript" src="js/image/java/FWDSimple3DCoverflow.js"></script>

		<!-- Setup the coverflow (all settings are explained in detail in the documentation files) -->
		<script type="text/javascript">
			FWDRLS3DUtils.onReady(function(){
				var coverflow = new FWDSimple3DCoverflow({

					//required settings
					coverflowHolderDivId:"myDiv2",
					coverflowDataListDivId:"coverflowData",
					displayType:"fluidwidth",
					autoScale:"yes",
					coverflowWidth:500,
					coverflowHeight:538,
					mainFolderPath:"js/image/load",
					skinPath:"skin_black",

					//main settings
					backgroundColor:"#DDDDDD",
					backgroundImagePath:"js/image/load/skin_black/main_skin/background.jpeg",
					backgroundRepeat:"repeat-x",
					showDisplay2DAlways:"no",
					coverflowStartPosition:"center",
					coverflowTopology:"dualsided",
					coverflowXRotation:0,
					coverflowYRotation:0,
					numberOfThumbnailsToDisplayLeftAndRight:"all",
					infiniteLoop:"no",
					rightClickContextMenu:"developer",
					useDragAndSwipe:"no",
					fluidWidthZIndex:1000,

					//thumbnail settings
					thumbnailWidth:400,
					thumbnailHeight:266,
					thumbnailXOffset3D:86,
					thumbnailXSpace3D:78,
					thumbnailZOffset3D:200,
					thumbnailZSpace3D:93,
					thumbnailYAngle3D:70,
					thumbnailXOffset2D:20,
					thumbnailXSpace2D:30,
					thumbnailHoverOffset:100,
					thumbnailBorderSize:10,
					thumbnailBackgroundColor:"#666666",
					thumbnailBorderColor1:"#fcfdfd",
					thumbnailBorderColor2:"#e4e4e4",
					transparentImages:"no",
					thumbnailsAlignment:"center",
					maxNumberOfThumbnailsOnMobile:13,
					showThumbnailsGradient:"yes",
					textBackgroundColor:"#333333",
					textBackgroundOpacity:.7,
					showText:"no",
					textOffset:10,
					showTextBackgroundImage:"yes",
					showThumbnailBoxShadow:"yes",
					thumbnailBoxShadowCss:"0px 2px 2px #555555",
					showTooltip:"no",
					dynamicTooltip:"yes",
					showReflection:"yes",
					reflectionHeight:60,
					reflectionDistance:0,
					reflectionOpacity:.2,

					//controls settings
					slideshowDelay:5000,
					autoplay:"no",
					disableNextAndPrevButtonsOnMobile:"no",
					controlsMaxWidth:650,
					controlsPosition:"center",
					controlsOffset:10,
					showLargeNextAndPrevCoverflowButtons:"yes",
					largeNextAndPrevButtonsOffest:20,
					showNextAndPrevCoverflowButtons:"no",
					showSlideshowButton:"no",
					showScrollbar:"no",
					showBulletsNavigation:"no",
					bulletsNormalColor:"#333333",
					bulletsSelectedColor:"#FFFFFF",
					bulletsNormalRadius:5,
					bulletsSelectedRadius:8,
					spaceBetweenBullets:10,
					bulletsOffset:14,
					disableScrollbarOnMobile:"yes",
					enableMouseWheelScroll:"yes",
					scrollbarHandlerWidth:200,
					scrollbarTextColorNormal:"#000000",
					scrollbarTextColorSelected:"#000000",
					addKeyboardSupport:"yes",

					//categories settings
					showCategoriesMenu:"no",
					startAtCategory:2,
					categoriesMenuMaxWidth:700,
					categoriesMenuOffset:25,
					categoryColorNormal:"#999999",
					categoryColorSelected:"#FFFFFF",

					//lightbox settings
					buttonsAlignment:"in",
					itemBoxShadow:"none",
					descriptionWindowAnimationType:"opacity",
					descriptionWindowPosition:"bottom",
					slideShowAutoPlay:"no",
					addKeyboardSupport:"yes",
					showCloseButton:"yes",
					showShareButton:"yes",
					showZoomButton:"yes",
					showSlideShowButton:"no",
					showSlideShowAnimation:"no",
					showNextAndPrevButtons:"yes",
					showNextAndPrevButtonsOnMobile:"yes",
					showDescriptionButton:"yes",
					showDescriptionByDefault:"no",
					videoShowFullScreenButton:"yes",
					videoAutoPlay:"yes",
					nextVideoOrAudioAutoPlay:"yes",
					videoLoop:"no",
					audioAutoPlay:"no",
					audioLoop:"no",
					backgroundOpacity:.9,
					descriptionWindowBackgroundOpacity:.95,
					buttonsHideDelay:3,
					slideShowDelay:4,
					defaultItemWidth:640,
					defaultItemHeight:480,
					itemOffsetHeight:50,
					spaceBetweenButtons:1,
					buttonsOffsetIn:2,
					buttonsOffsetOut:5,
					itemBorderSize:0,
					itemBorderRadius:0,
					itemBackgroundColor:"#333333",
					itemBorderColor:"#333333",
					lightBoxBackgroundColor:"#000000",
					descriptionWindowBackgroundColor:"#FFFFFF",
					videoPosterBackgroundColor:"#0099FF",
					videoControllerBackgroundColor:"#FFFFFF",
					audioControllerBackgroundColor:"#FFFFFF",
					timeColor:"#000000"
				});
			});
		</script>