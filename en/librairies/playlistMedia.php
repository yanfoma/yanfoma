<link rel="stylesheet" type="text/css"  href="js/video/content/global.css"/>
		<div id="myDiv"></div>

		<!--  Playlists 1-->
		<ul id="playlists" style="display:none;">

			<li data-source="list=PLQQ1hj-fTkVTIgIn1p9oQq72ZF3q4adI2"  data-thumbnail-path="js/video/content/thumbnails/large2.png">
				<p class="minimalDarkCategoriesTitle"><span class="bold">Title: </span>Yanfoma Videos Playlist</p>
				<p class="minimalDarkCategoriesDescription"><span class="bold">Description: </span>Videos Related to Yanfoma Production.</p>
			</li>
		</ul>

		<script type="text/javascript" src="js/video/java/FWDRVPlayer.js"></script>

		<!-- Setup video player-->
		<script type="text/javascript">
			FWDRVPUtils.onReady(function(){

				FWDRVPlayer.useYoutube = "yes";

				new FWDRVPlayer({
					//main settings
					instanceName:"player1",
					useYoutube:"yes",
					parentId:"myDiv",
					playlistsId:"playlists",
					mainFolderPath:"js/video/content",
					skinPath:"modern_skin_white",
					displayType:"responsive",
					facebookAppId:"213684265480896",
					autoScale:"yes",
					useDeepLinking:"no",
					rightClickContextMenu:"developer",
					addKeyboardSupport:"yes",
					stopVideoWhenPlayComplete:"no",
					autoPlay:"no",
					loop:"no",
					shuffle:"no",
					maxWidth:1480,
					maxHeight:800,
					showContextMenu:"yes",
					volume:.8,
					backgroundColor:"#000000",
					posterBackgroundColor:"transparent",
					//logo settings
					showLogo:"no",
					hideLogoWithController:"no",
					logoPosition:"bottomRight",
					logoLink:"http://yanfoma.tech/en/images/Yanfoma-logo.png",
					logoMargins:5,
					//playlists / categories settings
					showPlaylistsButtonAndPlaylists:"yes",
					showPlaylistsByDefault:"no",
					thumbnailSelectedType:"blackAndWhite",
					startAtPlaylist:0,
					buttonsMargins:7,
					thumbnailMaxWidth:350,
					thumbnailMaxHeight:350,
					horizontalSpaceBetweenThumbnails:40,
					verticalSpaceBetweenThumbnails:40,
					//playlist settings
					showPlaylistButtonAndPlaylist:"yes",
					playlistPosition:"right",
					showPlaylistByDefault:"yes",
					showPlaylistToolTips:"yes",
					forceDisableDownloadButtonForFolder:"yes",
					addMouseWheelSupport:"yes",
				    startAtRandomVideo:"no",
					folderVideoLabel:"Video ",
					startAtVideo:0,
					maxPlaylistItems:50,
					playlistToolTipMaxWidth:270,
					thumbnailWidth:120,
					thumbnailHeight:90,
					spaceBetweenControlerAndPlaylist:1,
					playlistToolTipFontColor:"#000000",
					//controller settings
					showButtonsToolTip:"yes",
					showControllerWhenVideoIsStopped:"yes",
					showVolumeScrubber:"yes",
					showVolumeButton:"yes",
					showTime:"yes",
					showLoopButton:"yes",
					showShuffleButton:"yes",
					showYoutubeQualityButton:"yes",
					showDownloadButton:"yes",
					showFacebookButton:"no",
					showEmbedButton:"yes",
					showFullScreenButton:"yes",
					repeatBackground:"no",
					buttonsToolTipHideDelay:1.5,
					controllerHeight:43,
					controllerHideDelay:3,
					startSpaceBetweenButtons:10,
					spaceBetweenButtons:10,
					mainScrubberOffestTop:14,
					scrubbersOffsetWidth:4,
					timeOffsetLeftWidth:1,
					timeOffsetRightWidth:2,
					timeOffsetTop:0,
					volumeScrubberWidth:80,
					volumeScrubberOffsetRightWidth:0,
					timeColor:"#888888",
					youtubeQualityButtonNormalColor:"#888888",
					youtubeQualityButtonSelectedColor:"#FFFFFF",
					buttonsToolTipFontColor:"#888888",
					//embed window
					embedWindowCloseButtonMargins:7,
					borderColor:"#333333",
					mainLabelsColor:"#51f9f6",
					secondaryLabelsColor:"#a1a1a1",
					shareAndEmbedTextColor:"#5a5a5a",
					inputBackgroundColor:"#000000",
					inputColor:"#FFFFFF",
					//ads
					openNewPageAtTheEndOfTheAds:"no",
					playAdsOnlyOnce:"no",
					adsButtonsPosition:"left",
					skipToVideoText:"You can skip to video in: ",
					skipToVideoButtonText:"Skip Ad",
					adsTextNormalColor:"#888888",
					adsTextSelectedColor:"#FFFFFF",
					adsBorderNormalColor:"#666666",
					adsBorderSelectedColor:"#FFFFFF"
				});
			});
		</script>