<section class="fact-counter sec-padd" style="background-image: url(images/background/4.jpeg);">
    <div class="container">
        <div class="row clearfix">
            <div class="counter-outer clearfix">
                <!--Column-->
                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                    <div class="item">
                        <div class="count-outer"><span class="count-text" data-speed="3000" data-stop="1">0</span></div>
                        <h4 class="counter-title">Open project</h4>
                        <div class="icon"><i class="icon-people"></i></div>
                    </div>

                </article>

                <!--Column-->
                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                    <div class="item">
                        <div class="count-outer"><span class="count-text" data-speed="3000" data-stop="5">0</span></div>
                        <h4 class="counter-title">Startup projects</h4>
                        <div class="icon"><i class="icon-technology3"></i></div>
                    </div>
                </article>

                <!--Column-->
                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                    <div class="item">
                        <div class="count-outer"><span class="count-text" data-speed="3000" data-stop="4">0</span></div>
                        <h4 class="counter-title">Studio projects</h4>
                        <div class="icon"><i class="icon-light"></i></div>
                    </div>
                </article>

                <!--Column-->
                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                    <div class="item">
                        <div class="count-outer"><span class="count-text" data-speed="3000" data-stop="1">0</span></div>
                        <h4 class="counter-title">Business Partnership Projects</h4>
                        <div class="icon"><i class="icon-landscape"></i></div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>