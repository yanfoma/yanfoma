<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>YanfoMedia || Advertisement Videos And Posters</title>
    <?php include_once("librairies/meta.php"); ?>
    <link rel="stylesheet" type="text/css" href="js/video/content/global.css">
</head>
<body>

<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<?php include_once("librairies/imagePlaylist.php"); ?>
<section class="whychoos-us sec-padd2">
    <div class="container">

        <div class="section-title center">
            <h2>Your WOW !!! is what we aim for. </h2>
            <div class="text">
                <p></p>
            </div>
        </div>

        <div class="row clearfix">
            <!--Featured Service -->
            <article class="column col-md-6 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-science-1"></span>
                    </div>
                    <a href="#"><h4>Advertisement Videos</h4></a>
                    <div class="text">
                        <p>You have a new product or a business you want to promot throught video spots? We are the best and your satisfication is our duty!!!!</p>
                    </div>
                    <div class="count">01</div>
                </div>
            </article>
            <!--Featured Service -->
            <article class="column col-md-6 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-computer"></span>
                    </div>
                    <a href="#"><h4>Posters And Logos</h4></a>
                    <div class="text">
                        <p>You're planning an event and you're looking for the right poster to attrack people?/? Don't look any further we got you.</p>
                    </div>
                    <div class="count">02</div>
                </div>
            </article>
        </div>
    </div>
</section>
<?php include_once("librairies/playlistMedia.php"); ?>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
</div>
</body>
</html>