<?php
    include("librairies/config.php");
    include("librairies/db.php");
    $query  = "SELECT * FROM career ORDER BY id DESC";
    $career = $db->query($query);
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>.:: Career || Yanfoma The hotpot of new technologies::.</title>
    <?php include_once("librairies/meta.php"); ?>
</head>
<style type="text/css">
    .lower-content{
        background-color: #eeeeee;
        margin-bottom: 2%;
    }
    .lower-content a{
        padding-left:2%;
    }
    .text{
        padding:2%;
    }
</style>
<body>

<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3>Yanfoma Job Panel</h3>
        </div><!-- /.box -->
    </div><!-- /.container -->
</div>
<div class="sidebar-page-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <section class="blog-section">
                    <?php while( $row = $career->fetch_assoc() ) {
                        ?>
                    <div class="large-blog-news wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="date"><?php  echo ($row['date']); ?></div>
                        <div class="lower-content">
                            <h4><a href="#"><?php echo ($row['title']); ?></a></h4>
                            <div class="text">
                                <p style="text-align:justify;"><?php echo ($row['body']); ?>
                                 </p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </section>
            </div>
            <?php include_once("librairies/sidebar.php"); ?>
        </div>
    </div>
</div>
<div class="call-out">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <figure class="logo">
                    <a href="index.php"><img src="images/logo/logo2.png" alt=""></a>
                </figure>
            </div>
            <div class="col-md-9 col-sm-12">
                <div class="float_left">
                    <h4>If you are interrested please get in touch with us!!!</h4>
                </div>
                <div class="float_right">
                    <a href="contact.php" class="thm-btn-tr">Go</a>
                </div>
            </div>
        </div>

    </div>
</div>

<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>

</div>

</body>
</html>