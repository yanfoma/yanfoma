<?php
    include("librairies/config.php");
    include("librairies/db.php");
    $query          = "SELECT * FROM categories";
    $categories     = $db->query($query);
    $tag_query      = "SELECT * FROM posts";
    $tags           = $db->query($tag_query);
    $latest_query   = "SELECT * FROM posts ORDER BY date DESC LIMIT 0,3 ";
    $news           = $db->query($latest_query);

     function countComments($count){
    if($count == 0 || $count == 1) echo $count.' Comment';
        else echo $count.' Comments';
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>..::Yanfoma || Technologies Provider::..</title>
    <?php include_once("librairies/meta.php"); ?>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '830980220396534');
      fbq('track', 'PageView');

      fbq('track', 'ViewContent', {
        value: 1,
        currency: '1',
      });
</script>

    <!-- End Facebook Pixel Code -->

</head>
<body>

<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<?php include_once("librairies/slideshow.php"); ?>
<?php include_once("librairies/services.php"); ?>
<?php include_once("librairies/counter.php"); ?>
<div class="container"><div class="border-bottom"></div></div>
<?php include_once("librairies/projects.php"); ?>
<?php //include_once("librairies/latestNews.php"); ?>
<?php //include_once("librairies/newsletter.php"); ?>
<?php //include_once("librairies/partners.php"); ?>

<div class="call-out">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <figure class="logo">
                    <a href="index.html"><img src="images/logo/logo2.png" alt=""></a>
                </figure>
            </div>
            <div class="col-md-9 col-sm-12">
                <div class="float_left">
                    <h4>You want to collabore with us. Contact us!!!</h4>
                </div>
            </div>
        </div>

    </div>
</div>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
</div>
</body>
</html>
