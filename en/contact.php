<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>..::Contact Us @ Yanfoma || The hotpot of new technologies::..</title>
    <?php include_once("librairies/meta.php"); ?>
</head>
<body>

<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<section class="contact_details sec-padd2">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item center">
                    <div class="icon_box">
                        <span class="icon-magnifying-glass"></span>
                    </div>
                    <h4><a href="https://www.facebook.com/yanfoma/">Visit Our Facebook Page</a></h4>
                    <div style="text-align:center">
                        <p><a href="https://www.facebook.com/yanfoma/">Like And Follow our Page on Facebook to be always updated.</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item center">
                    <div class="icon_box">
                        <span class="icon-clock"></span>
                    </div>
                    <h4>Get a quote and a discount</h4>
                    <div style="text-align:center">
                        <p>First, send us an email<br />
                          Second, get a quotation, <br />
                          Third, get a discount on this quote before October 31rst</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item center">
                    <div class="icon_box">
                        <span class="icon-telephone"></span>
                    </div>
                    <h4>Quick Contact</h4>
                    <div style="text-align:center">
                        <p>Infoline: +886 989 597 235 <br>Email: info@yanfoma.tech</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="contact_us sec-padd-bottom">
    <div class="container">
        <div class="call-out">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-12">
                        <div class="center">
                            <h4>Send Us A Message at yanfomatech@gmail.com Or on  </h4>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class=" center">
                            <a href="https://www.messenger.com/t/yanfoma" class="thm-btn-tr">facebook</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h3>Send Us a message at info@yanfoma.tech</h3>
                </div>
                <!--div class="default-form-area">
                    <form id="contact-form" name="contact_form" class="default-form" action="inc/sendmail.php" method="post">
                        <div class="row clearfix">
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="form-group commentaire">
                                    <input type="text" name="form_name" class="form-control" value="" placeholder="Your Name *" required="">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group commentaire">
                                    <input type="email" name="form_email" class="form-control required email" value="" placeholder="Your Mail *" required="">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group commentaire">
                                    <input type="text" name="form_phone" class="form-control" value="" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group commentaire">
                                    <input type="text" name="form_subject" class="form-control" value="" placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group commentaire">
                                    <textarea name="form_message" class="form-control textarea required" placeholder="Your Message...."></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ">
                                    <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                    <button class="thm-btn2" type="submit" data-loading-text="Please wait...">send message</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div-->
            </div>
            <div class="col-md-12 col-sm-8 col-xs-12">
                <div class="home-google-map">
                    <div
                        class="google-map"
                        id="contact-google-map"
                        data-map-lat="24.7897537"
                        data-map-lng="120.9965591"
                        data-icon-path="images/logo/map-marker.png"
                        data-map-title="Taiwan"
                        data-map-zoom="15" >
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
    <script src="js/owl.carousel.min.js"></script>
    <!-- jQuery validation -->
    <script src="js/jquery.validate.min.js"></script>
    <!-- google map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI"></script>
    <script src="js/gmap.js"></script>
</div>
</body>
</html>
