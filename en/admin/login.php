<?php
 session_start();
 include("librairies/config.php");
 include("librairies/db.php");
 $error = "";

 if(isset($_POST['login'])){

 	$email    = mysqli_real_escape_string($db, $_POST['email']);
 	$username = mysqli_real_escape_string($db, $_POST['username']);
 	$password = mysqli_real_escape_string($db, $_POST['password']);

 	/*echo "Your email    is $email".'<br>';
 	echo "Your username is $username".'<br>';
 	echo "Your password is $password".'<br>';*/

 	if(empty($email) | empty($username) |empty($password)){
 		$error = "Please All the fields are required";
 	}
 	if(!(empty($email)) && !(empty($username)) && !(empty($password))){
 		$query  = "SELECT * FROM admin WHERE email = '$email' AND username = '$username' AND password = '$password'";
 		$result = $db->query($query);
 		/*while($row 	= $result->fetch_assoc()){
 			echo $row['email'].'<br>';echo $row['username'].'<br>';echo $row['password'].'<br>';
 		}*/
 		if($result->num_rows == 1 ){
 			//echo "row num is $result->num_rows".'<br>';
			$_SESSION['email']    = $email;
			$_SESSION['username'] = $username;
			header("Location:index.php");
			exit();
 		} else{
 			header("location:login.php?err_msg=Wrong Credentials Try again!!!");
 			exit();
 		}
 	}
 }

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> Admin Board | Yanfoma The hotspot of technologies  </title>
	<!-- Bootstrap -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<!-- slimscroll -->
	<link href="assets/css/jquery.slimscroll.css" rel="stylesheet">
	<!-- Fontes -->
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/simple-line-icons.css" rel="stylesheet">
	<!-- all buttons css -->
	<link href="assets/css/buttons.css" rel="stylesheet">
	<!-- animate css -->
	<link href="assets/css/animate.css" rel="stylesheet">
<!-- adminbag main css -->
	<link href="assets/css/main.css" rel="stylesheet">
	<!-- aqua black theme css -->
	<link href="assets/css/aqua-black.css" rel="stylesheet">
	<!-- media css for responsive  -->
	<link href="assets/css/main.media.css" rel="stylesheet">
	<!--[if lt IE 9]> <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
	<!--[if lt IE 9]> <script src="dist/html5shiv.js"></script> <![endif]-->
</head>

<body class="aqua-bg login">
	<div class="logo">
		<a href="index.php"> <img src="assets/images/logo2.png" alt=""> </a>
	</div>
	<div class="middle-box text-center loginscreen ">
		<div class="widgets-container">
			<div class="bottom20"> <img alt="image" class="img-circle circle-border" src="assets/images/admin.jpg"> </div>
			<p>Yanfoma Admin Board</p>
			<span  style="color:red;"><?php if(isset($_GET['err_msg'])) echo $_GET['err_msg']; ?></span>
			<form method="post" class="top15">
				<div class="form-group">
					<input type="email" placeholder="email" 		name="email" class="form-control">
					<span  style="color:red;"><?php echo($errorEmail);?></span>
				</div>
				<div class="form-group">
					<input type="text" placeholder="Username" 		 name="username" class="form-control">
					<span  style="color:red;"><?php echo($errorUsername);?></span>
				</div>
				<div class="form-group">
					<input type="password" placeholder="Password" 	 name="password" class="form-control">
					<span  style="color:red;"><?php echo($errorPassword);?></span>
				</div>
				<button class="btn aqua block full-width bottom15" type="submit" name="login">Login</button>
			</form>
		</div>
	</div>
</body>

</html>