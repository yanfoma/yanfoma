<?php
  include_once("config.php");
  include_once("db.php");

  if( isset($_GET['entity']) && isset($_GET['action']) && isset($_GET['id']) ){
      $entity = mysqli_real_escape_string($db,$_GET['entity']);
      $action = mysqli_real_escape_string($db,$_GET['action']);
      $id     = mysqli_real_escape_string($db,$_GET['id']);

      if($action=="delete"){
        if($entity=="career"){
            $query= "DELETE FROM career WHERE id='$id'";
            $db->query($query);
        }
    }
  }

  $query = "SELECT * FROM career ORDER BY id DESC";
  $career=$db->query($query);

?>
<div class="row wrapper border-bottom page-heading">
  <div class="col-lg-12">
    <center><h2>Recent Carrer Posts (<?php echo($career->num_rows);?>)</h2></center>
  </div>
  <div class="col-lg-12"> </div>
</div>
<div class="wrapper-content ">
  <a class="btn btn-info" href="new_career.php"> <i class="fa fa-plus"></i> Add New Career </a>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content collapse in">
          <div class="widgets-container">
            <div >
              <table id="example" class="table  responsive nowrap table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Date Posted</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if($career->num_rows > 0){
                    while($row = $career->fetch_assoc() ) { ?>
                      <tr>
                        <td><?php echo $row['date'] ?></td>
                        <td><?php echo $row['title'] ?></td>
                        <td><?php echo "admin"; ?></td>
                        <td>
                          <a class="btn btn-warning "  href="new_career.php?career=<?php echo $row['id'] ?>" ><i class="fa fa-edit"></i> Edit</a>
                          <a class="btn btn-danger"    href="career.php?entity=career&action=delete&id=<?php echo $row['id'] ?>"> <i class="fa fa-times"></i> Delete </a>
                        </td>
                      </tr>
                    <?php }
                    } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
