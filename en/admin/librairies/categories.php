<div class="row wrapper border-bottom page-heading">
  <div class="col-lg-12">
    <center><h2>Categories</h2></center>
  </div>
  <div class="col-lg-12"> </div>
</div>
<div class="wrapper-content ">
  <a class="btn btn-info" href="new_category.php"> <i class="fa fa-plus"></i> Add New Category </a>
  <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <div class="ibox-content collapse in">
                <div class="widgets-container">
                  <div>
                  <form method="post">
                    <table id="example" class="table  responsive nowrap table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Select</th>
                          <th>ID</th>
                          <th>Title</th>
                          <th>Edit</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input type="checkbox" class="iCheck" name="checkbox[]"/></td>
                          <td>1</td>
                          <td>Java</td>
                          <td>
                            <a class="btn btn-warning"  href="#" ><i class="fa fa-edit"></i> Edit</a>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="checkbox" class="iCheck" name="checkbox[]"/></td>
                          <td>2</td>
                          <td>Web</td>
                          <td>
                            <a class="btn btn-warning"  href="#" ><i class="fa fa-edit"></i> Edit</a>
                          </td>
                        </tr>
                        <tr>
                          <td><input type="checkbox" class="iCheck" name="checkbox[]"/></td>
                          <td>3</td>
                          <td>Media</td>
                          <td>
                            <a class="btn btn-warning"  href="#" ><i class="fa fa-edit"></i> Edit</a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <select class="" name="action">
                    <option>Delete</option>
                    <option>Clone</option>
                  </select>
                  <button class="btn btn-default" type="submit" name="apply">Apply</button>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>