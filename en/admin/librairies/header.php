<?php
session_start();
if( !isset($_SESSION['email']) && !isset($_SESSION['username']) ){
	//echo "You have nothing ";
	echo "<script>alert('Error!!');</script>";
    echo "<script>window.location.href = 'login.php?err_msg=Please login To see the page you are loking for!!!'</script>";
	//header("location:../login.php?err_msg=Wrong Credentials Try again!!!");
 	//exit();
}
?>
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="index.php"> <img class="logo-default" alt="logo" src="assets/images/logoadmin.png"> </a>
		</div>
		<!-- END LOGO -->
		<div class="library-menu"> <span class="one">-</span> <span class="two">-</span> <span class="three">-</span> </div>
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- START USER LOGIN DROPDOWN -->
				<li class="dropdown dropdown-user">
					<a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
						<img src="assets/images/admin.jpg" class="img-circle" alt="">
						<span class="username username-hide-on-mobile"> Admin</span>							<i class="fa fa-angle-down"></i> </a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="logout.php"> <i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>