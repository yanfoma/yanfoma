<?php
  include_once("config.php");
  include_once("db.php");

  $query    = "SELECT * FROM comments ORDER BY id DESC";
  $comments =$db->query($query);

    if(isset($_GET['id']))
    {
      $id    = mysqli_real_escape_string($db,$_GET['id']);
      $body  = mysqli_real_escape_string($db, $_POST['body']);
      echo "Your id is $id and your reply is $body";
      //$query = "INSERT INTO comments (name,comment,post,email,dateComment,status,is_admin) VALUES ('Yanfoma','$reply',CURRENT_TIMESTAMP()')"
    }
    echo "your reply is $body";
?>
<div class="row wrapper border-bottom page-heading">
  <div class="col-lg-12">
    <center><h2>Comments</h2></center>
  </div>
  <div class="col-lg-12"> </div>
</div>
<div class="wrapper-content ">
<a class="btn btn-info" href="new.php"> <i class="fa fa-plus"></i> Add New Post </a>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content collapse in">
          <div class="widgets-container">
            <div>
              <form method="post">
                <table id="example" class="table  responsive nowrap table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Author</th>
                      <th>email</th>
                      <th>Post</th>
                      <th>Comment</th>
                      <th>Status</th>
                      <th>Reply</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?while($row = $comments->fetch_assoc()){
                      $id        = $row['post'];
                      $post      = $db->query("SELECT * FROM posts WHERE id ='$id' ");
                      $post      = $post->fetch_assoc();
                      $post_name = $post['title'];
                      ?>
                    <tr>
                      <td><?php echo $row['name']; ?></td>
                      <td><?php echo $row['email']; ?></td>
                      <td><?php echo $post_name; ?></td>
                      <td><?php echo $row['comment']; ?></td>
                      <?php if($row['status'] == 1) {
                        $status= "Approved";
                        $class = "success";
                      } else{
                        $status= "Pending...";
                        $class = "warning";
                      }

                      ?>
                      <td><button class="btn btn-<?php echo $class; ?>" type="button"><?php echo $status; ?> </button></td>
                      <td>
                        <textarea cols="105" rows="5" class="form-control" style="width:280px;"  name="body" id="body"></textarea><br>
                        <a href="comments.php?id=<?php echo $row['id']; ?>" class="btn btn-info" name="reply">Reply</a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>