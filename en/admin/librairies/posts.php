<div class="row wrapper border-bottom page-heading">
  <div class="col-lg-12">
    <center><h2>Posts</h2></center>
  </div>
  <div class="col-lg-12"> </div>
</div>
<div class="wrapper-content ">

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content collapse in">
          <div class="widgets-container">
            <div>
              <form method="post">
                <table id="example" class="table  responsive nowrap table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Date Posted</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Author</th>
                            <th>Tags</th>
                            <th>Category</th>
                            <th>Edit</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Jordan Belfort</td>
                            <td>System Architect</td>
                            <td>
                              <a class="btn btn-success" href="#" ><i class="fa fa-picture-o"></i> View</a>
                            </td>
                            <td>61</td>
                            <td>yanfoma</td>
                            <td>media</td>
                            <td>
                              <a class="btn btn-info "  href="#" ><i class="fa fa-edit"></i> Edit</a>
                            </td>
                          </tr>
                          <tr>
                            <td><input type="checkbox" class="iCheck" name="checkbox[]"/></td>
                            <td>Jordan Belfort</td>
                            <td>System Architect</td>
                            <td>
                              <a class="btn btn-success" href="#" ><i class="fa fa-picture-o"></i> View</a>
                            </td>
                            <td>61</td>
                            <td>yanfoma</td>
                            <td>media</td>
                            <td>
                              <a class="btn btn-info "  href="#" ><i class="fa fa-edit"></i> Edit</a>
                            </td>
                          </tr>
                          <tr>
                            <td><input type="checkbox" class="iCheck" name="checkbox[]"/></td>
                            <td>Jordan Belfort</td>
                            <td>System Architect</td>
                            <td>
                              <a class="btn btn-success" href="#" ><i class="fa fa-picture-o"></i> View</a>
                            </td>
                            <td>61</td>
                            <td>yanfoma</td>
                            <td>media</td>
                            <td>
                              <a class="btn btn-info "  href="#" ><i class="fa fa-edit"></i> Edit</a>
                            </td>
                          </tr>
                        </tbody>
                </table>
                <select class="" name="action">
                    <option>Delete</option>
                    <option>Clone</option>
                  </select>
                  <button class="btn btn-default" type="submit" name="apply">Apply</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>