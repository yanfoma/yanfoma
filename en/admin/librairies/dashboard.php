<?php
  include_once("config.php");
  include_once("db.php");

  if( isset($_GET['entity']) && isset($_GET['action']) && isset($_GET['id']) ){
      $entity = mysqli_real_escape_string($db,$_GET['entity']);
      $action = mysqli_real_escape_string($db,$_GET['action']);
      $id     = mysqli_real_escape_string($db,$_GET['id']);

      if($action=="delete"){
        if($entity=="post"){

            $query= "DELETE FROM posts WHERE id='$id'";

        } else if($entity=="comment"){

            $query= "DELETE FROM comments WHERE id='$id'";

        }else{
            $query= "DELETE FROM categories WHERE id='$id'";
            $q    = "UPDATE posts set category='0'  WHERE category='$id'";
      }
    } else{
      $query= "UPDATE comments set status='1' WHERE id='$id'";

      }
      $db->query($query);
      if(isset($q)){
        $db->query($q);
      }
  }

  $query = "SELECT * FROM posts ORDER BY id DESC";
  $posts=$db->query($query);

  $query_comments = "SELECT * FROM comments WHERE status='0' ORDER BY id DESC";
  $comments=$db->query($query_comments);

  $query_categories = "SELECT * FROM categories ORDER BY id DESC";
  $categories=$db->query($query_categories);

?>
<div class="row wrapper border-bottom page-heading">
  <div class="col-lg-12">
    <center><h2>Recent Blog Posts (<?php echo($posts->num_rows);?>)</h2></center>
  </div>
  <div class="col-lg-12"> </div>
</div>
<div class="wrapper-content ">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content collapse in">
          <div class="widgets-container">
            <div >
                <a class="btn btn-info" href="new_post.php"> <i class="fa fa-plus"></i> Add New Post </a>
              <table id="example" class="table  responsive nowrap table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Date Posted</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Author</th>
                    <th>Category</th>
                    <th>Tags</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if($posts->num_rows > 0){
                    while($row = $posts->fetch_assoc() ) { ?>
                      <tr>
                        <td><?php echo $row['date']; ?></td>
                        <td><?php echo $row['title']; ?></td>
                        <td>
                          <a class="btn btn-success" target="_blank" href="<?php echo "uploads/$row[image]"; ?>" ><i class="fa fa-picture-o"></i> View</a>
                        </td>
                        <td><?php echo $row['author']; ?></td>
                        <td><?php
                          $id_cat   = $row['category'];
                          $query    = "SELECT * FROM categories WHERE id = '$id_cat' ";
                          $cat_name = $db->query($query);
                          $cat_name = $cat_name->fetch_assoc();
                          if($cat_name['name'] == "") $cat = "No category";
                          else $cat = $cat_name['name'];
                        echo $cat; ?></td>
                        <td><?php echo $row['keywords'] ?></td>
                        <td style="width:15%">
                          <a class="btn btn-warning "  href="new_post.php?post=<?php echo $row['id']; ?>" ><i class="fa fa-edit"></i> Edit</a>
                          <a class="btn btn-danger" href="index.php?entity=post&action=delete&id=<?php echo $row['id']; ?>"> <i class="fa fa-times"></i> Delete </a>
                        </td>
                      </tr>
                    <?php }
                    } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--Comment section-->
  <div class="row wrapper border-bottom page-heading">
    <div class="col-lg-12">
      <center><h2>Recent Pending Comments (<?php echo($comments->num_rows);?>)</h2></center>
    </div>
    <div class="col-lg-12"> </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content collapse in">
          <div class="widgets-container">
            <div >
              <table id="example" class="table  responsive nowrap table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if($posts->num_rows > 0){
                    while($row = $comments->fetch_assoc() ) { ?>
                  <tr>
                    <td><?php echo $row['name'] ?></td>
                    <td><?php echo $row['comment'] ?></td>
                    <td style="width:17%">
                      <a class="btn btn-success "  href="index.php?entity=comment&action=approve&id=<?php echo $row['id'] ?>" ><i class="fa fa-edit"></i> Approve</a>
                      <a class="btn btn-danger"    href="index.php?entity=comment&action=delete&id=<?php echo $row['id'] ?>"> <i class="fa fa-times"></i> Delete </a>
                    </td>
                  </tr>
                   <?php }
                    } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div></div>
  <!--Categories section-->
    <div class="row wrapper border-bottom page-heading">
    <div class="col-lg-12">
      <center><h2>Categories (<?php echo($categories->num_rows);?>) </h2></center>
    </div>
      <div class="col-lg-12"> </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content collapse in">
          <div class="widgets-container">
            <div >
                <a class="btn btn-info" href="new_category.php"> <i class="fa fa-plus"></i> Add New Category </a>
              <table id="example" class="table  responsive nowrap table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if($posts->num_rows > 0){
                    while($row = $categories->fetch_assoc() ) { ?>
                  <tr>
                    <td><?php echo $row['id'] ?></td>
                    <td><?php echo $row['name'] ?></td>
                    <td style="width:15%">
                      <a class="btn btn-warning "  href="new_category.php?category=<?php echo $row['id'] ?>" ><i class="fa fa-edit"></i> Edit</a>
                      <a class="btn btn-danger"    href="index.php?entity=category&action=delete&id=<?php echo $row['id'] ?>"> <i class="fa fa-times"></i> Delete </a>
                    </td>
                  </tr>
                  <?php }
                    } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>