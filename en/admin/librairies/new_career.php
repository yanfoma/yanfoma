<?php
  include_once("config.php");
  include_once("db.php");

  if(isset($_POST['add_career'])){

      $title    = mysqli_real_escape_string($db, $_POST['title']);
      $body     = mysqli_real_escape_string($db, $_POST['body']);

      if(isset($_POST['id'])){
        $id     = mysqli_real_escape_string($db, $_POST['id']);
        $query  = "UPDATE career SET title='$title',body='$body' WHERE id='$id' ";
      }
      else{
        $d      = getDate();
        $date   = "$d[month] $d[mday] $d[year]";
        $query  = "INSERT INTO career(title,body,date)
                               VALUES('$title','$body','$date')";
      }

      $db->query($query);
      echo "<script>alert('Done!!');</script>";
      echo "<script>window.location.href = 'career.php'</script>";

  }

  if(isset($_GET['career'])){
    $id = mysqli_real_escape_string($db, $_GET['career']);
    $p  = $db->query("SELECT * FROM career WHERE id ='$id' ");
    $p  = $p->fetch_assoc();

  }
?>
<div class="row wrapper border-bottom page-heading">
  <div class="col-lg-12">
    <center><h2>Add New Career</h2></center>
  </div>
  <div class="col-lg-12"> </div>
</div>
<div class="wrapper-content ">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content collapse in">
          <div class="widgets-container">
            <form method="post">
              <?php if(isset($p)){
                    echo "<input type='hidden' value='$id' name='id' />";
                  }
              ?>
              <div class="form-group">
                <label control-label>Career Title:</label>
                <input class="form-control" type="text" value="<?php echo @$p['title'];?>" name="title" />
              </div>
              <div class="form-group">
                <textarea name="body" class="tinymce"><?php echo @$p['body'];?></textarea>
              </div>
              <button class="btn btn-default" type="submit" name="add_career">Add Career</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>