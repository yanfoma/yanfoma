<?php
  include_once("config.php");
  include_once("db.php");
  $error="";
  if(isset($_GET['category'])){
    $id    =  mysqli_real_escape_string($db,$_GET['category']);
    $query =  "SELECT * FROM categories WHERE id='$id' ";
    $c     = $db->query($query);
    $c     = $c->fetch_assoc();
  }

  if(isset($_POST['add_category'])){
    $category= mysqli_real_escape_string($db, $_POST['category']);
    if(isset($_GET['category'])){
      $query =  "UPDATE categories SET name='$category' WHERE id='$id' ";
      $db->query($query);
    }
      else{

          if(empty($category) | $category==" "){
            $error="Please Enter your category";
          }else if (is_numeric($category)){
            $error="Please Enter a text as category Numbers are not allowed!!!";
          }
          else{
            $query = "INSERT INTO categories (name) VALUES ('$category')";
            $db->query($query);
            echo "<script>alert('Done!!');</script>";
            echo "<script>window.location.href = 'index.php'</script>";
           }
      }
  }
?>
<div class="row wrapper border-bottom page-heading">
  <div class="col-lg-12">
    <center><h2>Add New Category:</h2></center>
  </div>
  <div class="col-lg-12"> </div>
</div>
<div class="wrapper-content ">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content collapse in">
          <div class="widgets-container">
            <form method="post">
              <div class="form-group">
                <label for="category" control-label>Category:</label>
                <input class="form-control" type="text" name="category" id="category" value="<?php echo $c['name'];?>"/>
                <span style="color:red;"><?php echo($error);?></span>
              </div>
              <button class="btn btn-default" type="submit" name="add_category">Add category</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>