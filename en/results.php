<?php
    include("librairies/config.php");
    include("librairies/db.php");
    if(isset($_GET['search'])){
        $keyword = mysqli_real_escape_string($db, $_GET['search']);
        $query   = "SELECT * FROM posts WHERE keywords LIKE '%$keyword%'";
        $posts   = $db->query($query);
        $title   = "Search Results for \"".$keyword."\"";
    }
    if(empty($keyword)){
        $title = "No Keywords found!!! Please enter a keyword in the search bar.";
    }
    if(!($posts->num_rows > 0)){
        $title = "No matching post. please try again!!!";
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>..::<?php echo $title;?> || Yanfoma The hotpot of new technologies::..</title>
    <?php include_once("librairies/meta.php"); ?>
</head>
<body>

<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3><?php echo($title).' ';?></h3>
        </div><!-- /.box -->
    </div><!-- /.container -->
</div>

<section class="blog-section sec-padd">
    <div class="container">
        <?php include_once("librairies/sidebar.php"); ?>
        <div class="row">
            <?php if($posts->num_rows > 0){
                while( $row = $posts->fetch_assoc() ){
                    ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                                <figure class="img-holder">
                                    <a href="single.php?post=<?php echo($row['id']); ?>">
                                        <img src="<?php
                                                if($row['image']=="") echo $default_image;
                                                else echo($row['image']);
                                            ?>" alt="News"></a>
                                    <figcaption class="overlay">
                                        <div class="box">
                                            <div class="content">
                                                <a href="single.php?post=<?php echo($row['id']); ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                                <div class="lower-content">
                                    <div class="date"><?php echo($row['date'])?></div>
                                    <h4><a href="single.php?post=<?php echo($row['id']); ?>"><?php echo($row['title'])?></a></h4>
                                    <div class="post-meta">
                                        by <?php
                                               if($row['author']=="") echo $default_author;
                                               else echo($row['author']);
                                            ?>  |
                                        Tag:<?php
                                               if($row['keywords']=="") echo $default_keyword;
                                               else echo($row['keywords']);
                                            ?> |
                                        category: <?php
                                                if($row['category']=='0') echo $default_category;
                                                else echo($row['category']);
                                               ?>
                                    </div>
                                    <div class="text">
                                        <p><?php $body = $row['body'];
                                            echo substr($body, 0, 100) ."...";
                                        ?></p>
                                    </div>
                                    <div class="link">
                                        <a href="single.php?post=<?php echo($row['id']); ?>" class="default_link">Read More <i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
             <?php }
            }?>
            </div>
        </div>
</section>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
</div>
</body>
</html>