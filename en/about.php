<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>..::About Us @ Yanfoma || The hotpot of new technologies::..</title>
    <?php include_once("librairies/meta.php"); ?>
    <link rel="stylesheet" href="js/lightbox/css/videobox.css" type="text/css" media="screen" />
</head>
<body>
<div class="boxed_wrapper">
<?php include_once("librairies/header.php"); ?>
<section class="whychoos-us sec-padd2">
    <div class="container">

        <div class="section-title center">
            <h2>Who we are</h2>
            <div class="text">
                <p>Yanfoma is a business startup founded in September 2016 which provide technology services and develop innovative products for those who have an organization (for profit or non-profit) and who need technology to take decisions, grow their activities and measure their performances, those who have a technology product and/or service and need a new market and those who have a technology idea and want to develop it. Yanfoma, gaining experiences, partners, opportunities, quickly evolved into a technology startup remotely operating in Ouagadougou (Burkina Faso).  Yanfoma considers itself as an African company, expanding its business to Nigeria and Rwanda in the following months.</p>
            </div>
        </div>
        <div class="row clearfix">
            <!--Featured Service -->
            <article class="column col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-science-1"></span>
                    </div>
                    <a href="#"><h4>Your growth is our target</h4></a>
                    <div class="text">
                        <p>We provide services for those who have an organization (for profit or non-profit) and who need technology to take decisions, grow their activities and measure their performances. We focus on technology, while our customers focus on their business, we build what they know they need and we tell them what they can do for their organization through technology</p>
                    </div>
                    <div class="count">01</div>
                </div>
            </article>
            <!--Featured Service -->
            <article class="column col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-computer"></span>
                    </div>
                    <a href="#"><h4>New market opportunities for your business</h4></a>
                    <div class="text">
                        <p>We provide services for those who have a technology product and/or service and need a new market. We help our customers localize their  product to attract customers, cut their expansion cost as we bring the same result, and spend less efforts marketing their products</p>
                    </div>
                    <div class="count">02</div>
                </div>
            </article>
            <!--Featured Service -->
            <article class="column col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="icon_box">
                        <span class="icon-money2"></span>
                    </div>
                    <a href="#"><h4>Let’s work together and make your idea true</h4></a>
                    <div class="text">
                        <p>We provide services for those who have a technology idea and want to develop it. We help them by connecting them with professionals of technology, connect with makers, manufacturers, developers, to make our innovators' technology ideas come true<br><br><br< p="">
                    </br<></p></div>
                    <div class="count">03</div>
                </div>
            </article>
        </div>
    </div>
</section>
<!--section class="default-section sec-padd">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="text-content">
                    <h4>We make it true</h4>
                    <div class="text">
                        <p>We all have ideas and dreams. We have dreams about the next killer app or the awesome product that will make us solve problems and earn money. Yanfoma is there to make your idea come true, to help your business become better and to contribute in having a happier community. All this through technologies</p>
                    </div>
                    <ul class="default-list">
                        <li><i class="fa fa-check-square-o"></i>Which toil and pain can procure great pleasure.</li>
                        <li><i class="fa fa-check-square-o"></i>Any right to find man who annoying.</li>
                        <li><i class="fa fa-check-square-o"></i>Consequences, avoids a pain that produces.</li>
                    </ul>
                    <div class="text">
                        <p>Who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <figure class="img-box">
                    <a href="#"><img src="images/resource/12.jpg" alt=""></a>
                </figure>
            </div>
        </div>
    </div>
</section-->
<section class="parallax center" style="background-image: url(images/background/1.jpg);">
    <div class="container">
        <h1>Discover Yanfoma In Few Minutes!!!</h1>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="video-holder">
            <div class="overlay-gallery">
                <div class="icon-holder">
                    <div class="icon">
                        <a class="html5lightbox thm-btn" title="Yanfoma Introduction" href="https://www.youtube.com/watch?v=-xDcvGWgTNk">View</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="service-style3 sec-padd2">
    <div class="container">
        <div class="section-title">
            <h2>How we do it</h2>
        </div>
        <div class="service_carousel">
            <div class="item">
                <div class="bottom-content">
                    <div class="icon_box">
                        <span class="icon-platform"></span>
                    </div>
                    <a href="#"><h4>IDEA</h4></a>
                    <p class="title">Innovative</p>
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                        <span class="icon-platform"></span>
                        </div>
                        <a href="#"><h4>IDEA</h4></a>
                        <div class="text">
                            <p>If you have an interesting idea that you want to realise or prototype, you can come and join us.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="bottom-content">
                    <div class="icon_box">
                        <span class="icon-phone-call"></span>
                    </div>
                    <a href="#"><h4>Contact Us</h4></a>
                    <p class="title">Don't Hesitate</p>
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                        <span class="icon-phone-call"></span>
                        </div>
                        <a href="#"><h4>Contact Us</h4></a>
                        <div class="text">
                            <p>Get in touch with us by email and attach your idea. We will give you some advice and analyze the possibility for you to join yanfoma.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="bottom-content">
                    <div class="icon_box">
                        <span class="icon-people"></span>
                    </div>
                    <a href="#"><h4>Join Us</h4></a>
                    <p class="title">Be Part of Yanfoma</p>
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                        <span class="icon-people"></span>
                        </div>
                        <a href="#"><h4>Join Us</h4></a>
                        <div class="text">
                            <p>When you join yanfoma, you get our full support and become the manager of your project. So you become the boss, we just guide you.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="bottom-content">
                    <div class="icon_box">
                        <span class="icon-business"></span>
                    </div>
                    <a href="#"><h4>Collaborate with Us</h4></a>
                    <p class="title">Business Partnership</p>
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                        <span class="icon-business"></span>
                        </div>
                        <a href="#"><h4>Collaborate with Us</h4></a>
                        <div class="text">
                            <p>We can sign a partnership in order to help each other develop local or international Business. We encourage win-win partnership</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php //include_once("librairies/faq.php"); ?>
<?php include_once("librairies/footer.php"); ?>
<?php include_once("librairies/script.php"); ?>
    <!-- Html 5 light box script-->
<script src="js/html5lightbox/html5lightbox.js"></script>
</div>
</body>
</html>
